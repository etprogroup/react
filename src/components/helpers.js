﻿import {DOMAIN} from '../config'

export function getToken(callback){
    fetch(
        DOMAIN+'/api/oauth/token', {
            method: 'post',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                "grant_type": "password",
                "client_id": "web-ui",
                "client_secret": "A$3cr3tTh4tM4tt3r$T0U$",
                "username": "test-user",
                "password": "T3estUs3rP4ssw0rd$1"
            })
        })

        .then((response) => {
            return response.json();
        })
        .then((responseData) => {
            callback(responseData);
        })
        .catch(function(e) {
            console.log(e)
    });
}

export function multiselectValueToArray(value){
    var array=[];
    value.map(function(a){
        array.push(a.value)
    });
    return array
}

export function arrayToMultiselectValue(value){
    if(typeof (value)=='undefined')return []
    var array=[];
    value.map(function(a){
        array.push({"value":a})
    });
    return array
}

export function postImage(file,callback){

    if(!file){return callback(null)}

    return new Promise((resolve, reject) => {

        let imageFormData = new FormData();

        imageFormData.append('image', file);

        var url = DOMAIN+'/api/image-upload/';
        fetch(
            url, {
                method: 'post',
                body: imageFormData
            })
            .then(res => res.json())
            .then(results => {
                console.log(results)
                const source =  results.imageUrl;
                callback(source)
            })
            .catch(function(e) {
                console.log(e);
            });


    });

}

/**
 * @return {boolean}
 */
export function validateEmail(inputText)
{
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/;
    return inputText.match(mailformat) != null;
}

export function slugify (text) {
    if(!text)return '';

    const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
    const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return text.toString()
        .toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(p, c =>
            b.charAt(a.indexOf(c)))     // Replace special chars
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')             // Trim - from end of text
}