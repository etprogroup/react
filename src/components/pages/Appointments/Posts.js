import React, { Component } from 'react';
import {Link} from 'react-router-dom'

var postNumber;

class Posts extends Component {
    constructor(props){
        super(props);

        postNumber= this.props.posts ? this.props.post : -1;//We set -1 because it's an index, later we add +1 for the loop

    }

    render(){

        if (!this.props.images[0]) {
            return (
            <div style = {styles.loaderContainer}>
                <h3 style={styles.loader}>{this.props.message}</h3>
            </div>
        )}

        var postRows = [];
        for (var i=0; i < this.props.numberOfPosts; i++) {
            const link = this.props.posts[i].link;
            postRows.push(
                <div
                    style = {styles.postWrapper}
                    key = {this.props.posts[i].id}>
                    <div style = {styles.postTexts}>
                        <h1  style = {styles.postH1}>{this.props.posts[i].title.rendered.toUpperCase()}</h1>
                        <h4  style = {styles.postH2}>{this.props.posts[i].excerpt.rendered.substring(3,100)}...</h4>
                    </div>
                    <img
                       src = {this.props.images[i]}
                       className="post-image"
                    />
                    <h4
                        style = {styles.postButton}
                        onClick = {(e) => window.open( link, '_blank' )}
                    >Continuar</h4>
                </div>
            );
        }

        return <div style = {styles.container}>{postRows}</div>;
    }
}

const styles={
    container: {
        flex: 1,
    },
    postWrapper:{
        flex: 1,
        marginTop:15
    },
    postH2:{
        color:'white',
        fontFamily: 'helveticaTh',
        padding:5,
        paddingTop:0,
        fontSize:12,
        textAlign:'left',
        marginBottom:0
    },
    postH1:{
        color:'white',
        fontFamily: 'helveticaBdC',
        padding:5,
        paddingBottom:0,
        fontSize:22,
        textAlign:'left'
    },
    postTexts:{
        paddingTop:20,
        backgroundColor:'#343b45'
    },
    postButton:{
        color:'white',
        padding:7,
        borderWidth:1,
        borderColor:'white',
        width:93,
        fontFamily: 'helveticaTh',
        fontSize:18,
        textAlign:'center',
        marginTop:-50,
        position:'absolute',
        marginLeft:20,
        backgroundColor: 'rgba(0,0,0,0.3)',
        borderStyle: 'solid',
        cursor:'pointer'
    },
    loader:{
        textAlign:'center',
        fontSize:12,
        width:'100%',
        fontFamily: 'helvetica_th',
        color:'white',
        paddingTop:200
    },
    loaderContainer:{
        justifyContent:'center'
    }
};

export default Posts;