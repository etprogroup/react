﻿import React, { Component } from 'react';
import { BrowserRouter as Router,
    Route,
    Redirect,
    Switch
} from 'react-router-dom';
import DayCalendar from "./DayCalendar";
import Moment from 'moment';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import SuperSelectField from '../../SuperSelectField'
import CircularProgress from 'material-ui/CircularProgress/CircularProgress'
import ActionShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add';
import NavigationCancel from 'material-ui/svg-icons/navigation/cancel';
import {multiselectValueToArray} from '../../helpers'
import {arrayToMultiselectValue} from '../../helpers'
import {parseAndFilterEvents} from './utils/parseAndFilterEvents'
import _arrayCopy from 'lodash._arraycopy';
import CashRegister from './CashRegister'
import {db} from '../../db'
import Chip from 'material-ui/Chip';
import {grey700,teal700} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import SpainLogo from '../../../spain.svg';
import BrasilLogo from '../../../brazil.svg';
import UKLogo from '../../../united-kingdom.svg';
import IconButton from 'material-ui/IconButton';
import Clear from 'material-ui/svg-icons/content/clear';
import Snackbar from 'material-ui/Snackbar';
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'

import './style.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';

const style = {
    margin: 8
};

const styles={
    checkbox:{
        color:'#666',
        width:280,
        display:'inline-flex',
        marginTop:3
    },
    checkboxPresupuesto:{
        color:'#666',
        textAlign:'right',
    },
    checkboxIcon:{
        color:'#666',
        marginRight:10
    },
    button:{
        marginRight:50,
        display:'inline-flex'
    },
    timepickers:{
        marginLeft:10
    },
    timepickersTF:{
        width:60
    },
    chip: {
        margin: 4,
        display: 'inline-block'
    },
    unconfirmedMessageStyle:{
        backgroundColor: '#ff4300',
        fontWeight: 'bold',
        fontSize: 15,
        color: '#fff',
        padding: 5,
        textAlign: 'center',
        width: 250
    }
};

class Calendar extends Component{

    constructor(props){
        super(props);

        this.morningClosing=null;
        this.eveningClosing=null;
        this.eveningStart=null;
        this.morningStart=null;
        this.slots=0;

        this.state = {
            calendarDay:new Date(),
            open: false,
            openCash: false,
            openClosingTimeConfirmation: false,
            openConfirmDeleting: false,
            isRedirect: false,
            appointment:{},
            employeeFilter:['Todos'],
            customerFilter:['Todos'],
            serviceFilter:['Todos'],
            formIniTime:null,
            formEndTime:null,
            formDate:null,
            formEmployee:null,
            shop:this.props.salon,
            servicePrice: null,
            cancelOrPay:null,
            eventClosingConfirmed:false,
            scheduled:false,
            servicesDetails:false,
            cashTab:'a',
            automaticAppointment:false,
            formIniTimeError:false,
            phoneNumbererror:null,
            servicesError:false,
            customerError:false,
            endTimeError:null,
            openSnackbar: false,
            openActionSnackbar: false
        };
        this.saveNewCustomer= this.saveNewCustomer.bind(this)
        this.handleServicesSelection= this.handleServicesSelection.bind(this)
        this.deleteConfirmation= this.deleteConfirmation.bind(this)
        this.verifyTill= this.verifyTill.bind(this)
        this.handleActionClick= this.handleActionClick.bind(this)
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    }

    forceUpdateHandler(){
        this.forceUpdate();
    };

    verifyTill(){
        var {handleChangeList} = this.props
        if(!handleChangeList){
            var endDate = new Date();
            endDate.setDate(endDate.getDate()+1);
            var tomorrowFormatted = Moment(endDate).format('YYYY-MM-DD');
            var query = '?dateIni=' + Moment(Date.now()).format('YYYY-MM-DD') + '&dateEnd=' + tomorrowFormatted;
            db('get','/api/tills/shop/' + this.props.salon + query,this.props.token)
                .then((responseData) => {
                    if(responseData){
                        var stateInit = (responseData.length === 0 || responseData[responseData.length - 1].event === "Close") ? true : false
                        if(stateInit){
                            this.timer = setTimeout(() => {
                                this.setState({
                                    open:false,
                                    snackActionMessage: <p style={{display:'inline-block', marginTop:18}}><Translate>La caja esta cerrada. ¿Desea abrirla?</Translate></p>,
                                    openActionSnackbar: true
                                });
                            }, 1500);
                        }
                    }
                }).catch(function(err) {
                console.log(err);
            });
        }
    }

    getNearEventFinishTime = (slotIniTime,dresserId)=>{

        const {appointments} = this.state

        let dresserEvents = appointments.filter((appointment)=>{
            if(appointment.status==='Cancelado' || appointment.status==='Eliminado')return false
            if(appointment.employee)return dresserId === appointment.employee.name
            return false
        })

        dresserEvents.map((event)=>{
            let diff=this.getMinutesDiff(slotIniTime,event.end);
            if(diff<30&&diff>-30){
                slotIniTime = Moment.utc(slotIniTime).subtract(diff,'minutes').toDate()
            }
        })
        return slotIniTime
    }

    getNextEventStartTime = (slotIniTime,dresserId)=>{
        const {appointments} = this.state

        let dresserEvents = appointments.filter((appointment)=>{
            if(appointment.status==='Cancelado' || appointment.status==='Eliminado')return false
            if(appointment.employee)return dresserId === appointment.employee.name
            return false
        })

        let nextEvent=new Date(2100,1,1);
        let minDiff=10000;
        dresserEvents.map((event)=>{
            let diff=this.getMinutesDiff(event.start,slotIniTime);
            if(diff<minDiff && diff>0){
                minDiff=diff;
                nextEvent=event;
            }
        })

        return nextEvent.start;
    }

    getMinutesDiff = (endDate,iniDate)=>{
        return (
                Moment
                    .utc(Moment(new Date(endDate),"DD/MM/YYYY HH:mm:ss")
                    .diff(Moment(new Date(iniDate),"DD/MM/YYYY HH:mm:ss"))
                )
                    .valueOf())/(1000*60)
    }

    getDressers = (newDate) =>{

        var employee = this.state.appointment ? this.state.appointment.employee : '';
        var calendarDay = newDate ? newDate : this.state.calendarDay;
        var offset = Moment().utcOffset();
        var calendarDayFormatted = Moment.utc(calendarDay).utcOffset(offset).format('DD-MM-YYYY');
        db('get','/api/employees/shop/'+this.props.salon,this.props.token)
            .then((responseData) => {
                let dressers = responseData.filter((v)=>{
                    return v.roles==='Estilista';
                });

                let employeesList = dressers.filter((v)=>{
                    var isLaboralDay = v.holidays ? v.holidays.indexOf(calendarDayFormatted) === -1 : true
                    if(v.name && v.name === employee && !isLaboralDay)
                        employee = '';
                    return (v.roles==='Estilista' && isLaboralDay);
                });

                var employees = employeesList.map(({name,_id}) =>
                    <div key={_id} value={name}>{name}</div>
                );
                this.setState({
                    dressers:dressers,
                    employees:employees,
                    appointment: { ...this.state.appointment, employee: employee }
                },reactTranslateChangeLanguage(this.props.user.language));
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(e) {
                console.log(e);
            });
    }

    getAppointments = ()=>{
        const {calendarDay} = this.state;
        var offset = Moment().utcOffset();
        var todayFormatted = Moment.utc(calendarDay).utcOffset(offset).format('YYYY-MM-DD');
        db('get','/api/appointments/shop/'+this.props.salon+'?date='+todayFormatted,this.props.token)
            .then((responseData) => {
                var appointmentCount = 0;
                for (var i = 0; i < responseData.length; i++) {
                    if(responseData[i].status === 'Pendiente'){
                        appointmentCount = appointmentCount +1;
                    }
                }
                this.setState({
                    appointments:responseData,
                    appointmentUnconfirmed: appointmentCount
                },reactTranslateChangeLanguage(this.props.user.language));
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(e) {
                console.log(e);
            });
    };

    getCustomers(){
        db('get','/api/customers/shop/'+this.props.salon,this.props.token)
            .then((responseData) => {
                if(responseData){
                    var nodes = responseData.map(({name, surname, phoneNumber, _id}) =>
                        <div key={_id} value={phoneNumber} label={surname ? name + " " + surname : name}>{name} {surname}<span style={{color:'blue'}}>- {phoneNumber}</span></div>
                    );
                    this.setState({customers:nodes,allCustomers:responseData})
                }
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(e) {
                console.log(e);
            });
    }

    getProducts(){
        db('get','/api/products/',this.props.token)
            .then((response) => {
                this.setState({allProducts:response});
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(e) {
                console.log(e);
            });
    }

    getServices(inputCustomer, isAll){
        // if customer exist
        var customer = this.state.appointment ? this.state.appointment.customer : null;
        if(this.state.appointment && this.state.appointment.anonymousCustomer){
            isAll = true;
        }
        if(inputCustomer){
            customer = inputCustomer;
            isAll = false;
        }
        if(isAll || customer === null){
            db('get','/api/shops/'+this.props.salon,this.props.token)
                .then((responseData) => {
                    var services = responseData.shop.services;
                    var nodes = services.map(({name, _id}) =>
                        <div key={_id} value={name}>{name}</div>
                    )
                    var value = 0;
                    var servicesSelected = this.state.appointment.services;
                    if(servicesSelected && servicesSelected.length > 0){
                        var service = [];
                        servicesSelected.forEach(function (element) {
                            service = services.filter((serv)=>{
                                return serv.name === element
                            })
                            value = value + service[0].price;
                        });
                    }
                    this.setState({services:nodes,allServices:responseData.shop.services, servicesDetails:false, servicePrice: value},reactTranslateChangeLanguage(this.props.user.language));
                })
                .then(() => reactTranslateChangeLanguage(this.props.user.language))
                .catch(function(e) {
                    console.log(e);
                });
        }
        else{
            if (customer !== null){
                //Validating customer
                var allCustomers = this.state.allCustomers? this.state.allCustomers : []
                var selectedCustomer = allCustomers.filter(function(value){
                    return value.phoneNumber===customer
                })
                selectedCustomer = selectedCustomer[0];
                if(!selectedCustomer){
                    selectedCustomer=null;
                }else{
                    selectedCustomer=selectedCustomer._id;
                    db('get', '/api/shops/'+this.props.salon+'/customer/'+selectedCustomer, this.props.token)
                        .then((responseData) => {
                            if(responseData){
                                var nodes = responseData.map((value) =>
                                    <div key={value.service._id} value={value.service.name} label={value.service.name + " - " + value.service.count}>{value.service.name} <span style={{color:'blue'}}>- {value.service.count} - ({value.lastPrice}€)</span></div>
                                )
                                var services = [];
                                responseData.map((v)=>{
                                    var serv = v.service;
                                    serv.lastPrice = v.lastPrice;
                                    serv.count = v.count;
                                    serv.days = v.days;
                                    serv.lastDate = v.lastDate;
                                    services.push(serv);
                                });

                                var value = 0;
                                var servicesSelected = this.state.appointment.services;
                                if(servicesSelected && servicesSelected.length > 0){
                                    var service = [];
                                    servicesSelected.forEach(function (element) {
                                        service = services.filter((serv)=>{
                                            return serv.name === element
                                        })
                                        value = value + (service[0].lastPrice ? service[0].lastPrice : service[0].price);
                                    });
                                    if(service.length && service[0].days) {
                                        var days = service[0].days;
                                        var offset = Moment().utcOffset();
                                        var lastDate = service[0].lastDate;
                                        var lastDateFormatted = Moment.utc(lastDate).utcOffset(offset).format('YYYY-MM-DD');
                                        var serviceDetails = true;
                                    }else var serviceDetails = false;
                                }
                                this.setState({services:nodes,allServices:services, days:days, lastDate:lastDateFormatted, servicesDetails:serviceDetails, servicePrice: value},reactTranslateChangeLanguage(this.props.user.language));
                            }
                        })
                        .then(() => reactTranslateChangeLanguage(this.props.user.language))
                        .catch(function(e) {
                            console.log(e);
                        });
                }
            }
        }
    }

    getClosing = ()=>{
        db('get','/api/shops/'+this.props.salon,this.props.token)
            .then((responseData) => {
                this.setState({
                    opening:responseData.shop.opening,
                    holidays:responseData.shop.holidays
                },reactTranslateChangeLanguage(this.props.user.language))
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(e) {
                console.log(e);
            });
    };

    onDaySelect(newDate){
        this.setState({appointments:[]})
        this.setState({calendarDay:newDate},()=>this.getAppointments())
        this.getDressers(newDate);
    }

    closeNewAppointment() {
        this.setState({ open: false });
    }

    closeRegister() {
        this.getAppointments();
        this.setState({ openCash: false });
    }

    handleRequestCloseSnackbar = () => {
        this.setState({
            openSnackbar: false,
            openActionSnackbar: false
        });
    };

    openNewAppointment(slotInfo){
        var {employee,formIniTime} = slotInfo;
        var maxEventDate = this.getNextEventStartTime(formIniTime,employee)
        var formIniTimeFixed = this.getNearEventFinishTime(formIniTime,employee)

        this.setState({
            open: true,
            newCustomer:false,
            newServices:false,
            newForm:true,
            appointment:{
                employee:employee,
                anonymousCustomer:false,
                customer: ""
            },
            formIniTime:formIniTimeFixed,
            formEndTime:null,
            servicesError:false,
            customerError:false,
            endTimeError:null,
            selectedCustomer: null,
            maxEventDate:maxEventDate,
            openClosingTimeConfirmation: false,
            eventClosingConfirmed:false,
            formIniTimeError:false,
            phoneNumbererror:null,
            customer: { ...this.state.customer, phonePrefix: null, phoneNumber:null }
        }, reactTranslateChangeLanguage(this.props.user.language),this.getServices(null, true));
    }

    editAppointment(event){
        if(event.status !== "Libre"){
            const {allServices} = this.state
            var appointment=this.state.appointments.filter(function(value){
                return value._id===event._id
            })

            appointment=appointment[0]
            var services = [];
            var customer = appointment.customer ? appointment.customer.phoneNumber : null;
            var customerName = appointment.customer ? (appointment.customer.surname ? (appointment.customer.name + " " + appointment.customer.surname) : appointment.customer.name) : null;
            var employee = appointment.employee ? appointment.employee.name : null;
            if(appointment.services){
                appointment.services.forEach(function (value2) {
                    let service = allServices.filter((srv)=>{
                        return value2===srv._id
                    });
                    if(service.length > 0){
                        services.push(service[0].name);
                    }
                });
            }
            var iniTime = new Date(appointment.start)
            var endTime = new Date(appointment.end)
            this.setState({
                appointment:{
                    ...appointment,
                    employee:employee,
                    customer:customer,
                    customerName: customerName,
                    services:services,
                    start:iniTime,
                    end:endTime,
                    _id:appointment._id
                },
                open:true,
                newForm:false,
                formIniTime:iniTime,
                formEndTime:endTime,
                newCustomer:false,
                customerError:false,
                selectedCustomer: null,
                endTimeError:null,
                servicesError:false,
                formIniTimeError:false,
                phoneNumbererror:null,
                cashTab:'a',
                cancelOrPay:null
            }, reactTranslateChangeLanguage(this.props.user.language), this.getServices(customer, false));
        }
    }

    saveNewCustomer(appointment){
        var validated=true;
        const {phoneNumber,phonePrefix,name,surname, email}=this.state.customer;
        var completePhone = phonePrefix ? phonePrefix.concat(phoneNumber) : null;

        if(completePhone === null){
            validated=false;
            this.setState({phoneNumbererror:(<div style={{color:'red',fontSize:12,marginTop:5}}><Translate>El prefijo es obligatorio</Translate></div>)})
        }
        else if(this.state.customer.phoneNumber === null || this.state.customer.phoneNumber === "" || typeof (this.state.customer.phoneNumber) === "undefined"){
            validated=false;
            this.setState({phoneNumbererror:(<div style={{color:'red',fontSize:12,marginTop:5}}><Translate>El número de télefono es obligatorio</Translate></div>)})
        }else{
            this.setState({phoneNumbererror:null})
        }

        var body={
            phoneNumber: completePhone,
            name:name,
            surname:surname,
            email:email,
            shop:this.props.salon
        }

        if(validated){
            db('post','/api/customers/',this.props.token,body)
                .then((responseData) => {
                    if(responseData){
                    if(responseData.customer){
                        this.timer = setTimeout(() => {
                            this.setState({
                                snackMessage: <Translate>Cliente creado correctamente.</Translate>,
                                openSnackbar: true
                            });
                        }, 2000);
                        appointment.customer=responseData.customer._id;
                        this.saveNewAppointment(appointment)
                        this.getCustomers();
                    }
                    else{
                        this.setState({
                            open:false,
                            snackMessage: <Translate>Ha ocurrido un error al guardar el nuevo cliente.</Translate>,
                            openSnackbar: true
                        });
                        }
                    }
                    else{
                        this.setState({
                            open:false,
                            snackMessage: <Translate>Ha ocurrido un error al guardar el nuevo cliente.</Translate>,
                            openSnackbar: true
                        });
                    }
                })
                .then(() => reactTranslateChangeLanguage(this.props.user.language))
                .catch(function(err) {
                    console.log(err);
                    this.setState({
                        snackMessage:<Translate>Ha ocurrido un error inesperado: </Translate>+{err},
                        openSnackbar:true,
                        open:false
                    });
                });
        }
    }

    deleteConfirmation() {
        this.setState({
            openConfirmDeleting:true,
            open:false
        }, reactTranslateChangeLanguage(this.props.user.language))
    }

    validateData(){
        const {eventClosingConfirmed} = this.state

        var validated=true;

        const {
            newCustomer,
            allServices,
            formIniTime,
            formEndTime,
            formDate,
            }=this.state;

        const {services,employee,customer,anonymousCustomer} = this.state.appointment;

        var calendarDay = this.state.calendarDay;
        if(formIniTime.getHours() < calendarDay.getHours() || (formIniTime.getHours() === calendarDay.getHours() && formIniTime.getMinutes() < calendarDay.getMinutes())){
            this.setState({formIniTimeError: true},reactTranslateChangeLanguage(this.props.user.language));
            validated = false;
        }
        else {
            this.setState({formIniTimeError: false})
        }
        //Validating Services
        if(services && services.length){

            var selectedServicesArray = [];
            services.map((value)=>{
                let service = this.state.allServices.filter((value2)=>{
                    return value2.name===value;
                });
                selectedServicesArray.push(service[0]._id)
            });
        }else{
            this.setState({newServices: false,servicesError:true},reactTranslateChangeLanguage(this.props.user.language))
            validated = false;
        }
        if(!formEndTime)validated=false;

        //Validating customer
        var selectedCustomer = this.state.allCustomers.filter(function(value){
            return value.phoneNumber===customer
        })
        selectedCustomer = selectedCustomer[0];
        if(!selectedCustomer){
            selectedCustomer=null;
        }else{
            selectedCustomer=selectedCustomer._id;
        }

        if(!selectedCustomer && !newCustomer && anonymousCustomer === false){
            this.setState({customerError:true})
            validated = false;
        }

        let eventFinish = formEndTime ? parseInt(formEndTime.getHours().toString() + formEndTime.getMinutes().toString()) : 0

        if(eventFinish>this.eveningClosing || (this.morningClosing && (eventFinish>this.morningClosing && eventFinish<this.eveningStart)))
        {
                if(!eventClosingConfirmed){
                    this.setState({openClosingTimeConfirmation:true,open:false})
                    return false;
                }
        }

        //Getting dresser id
        var dresser = this.state.dressers.filter(function(value){
            return value.name===employee
        })
        dresser = dresser[0];

        // Miramos si se ha cambiado la fecha original mediante el DatePicker
        if(formDate){
            // Asignamos el dia del formIniTime/formEndTime al nuevo escogido por el usuario
            formIniTime.setDate(formDate.getDate());
            formIniTime.setMonth(formDate.getMonth());
            formIniTime.setFullYear(formDate.getFullYear());
            formEndTime.setDate(formDate.getDate());
            formEndTime.setMonth(formDate.getMonth());
            formEndTime.setFullYear(formDate.getFullYear());

        }


        var appointment={
            "employee":dresser._id,
            "customer":selectedCustomer,
            "services":selectedServicesArray,
            "start": formIniTime,
            "end":formEndTime,
            "shop":this.props.salon,
            "source":"Shop",
            "status":"Confirmado",
            "observaciones":this.state.appointment.observaciones,
            "proposedServicesPriceSentToClient":this.state.appointment.proposedServicesPriceSentToClient,
            "anonymousCustomer":this.state.appointment.anonymousCustomer,
            "images":this.state.appointment.images
        };

        if(validated){
            if(newCustomer){
                this.saveNewCustomer(appointment);
            }else{
                this.saveNewAppointment(appointment);
            }
        }
    }

    saveNewAppointment(appointment){

        var method = this.state.newForm ? 'post' : 'put';
        db(method, '/api/appointments/' + (this.state.newForm ? '' : this.state.appointment._id), this.props.token, appointment)
            .then((response) => {
                if(response) {
                    this.setState({
                        open: false,
                        snackMessage: this.state.newForm ? <p><Translate>Cita creada correctamente.</Translate></p> : <p><Translate>Cita modificada correctamente.</Translate></p> ,
                        openSnackbar: true,
                        appointment: response.appointment
                    },reactTranslateChangeLanguage(this.props.user.language));
                }
                else {
                    this.setState({
                        open:false,
                        snackMessage: this.state.newForm ? <p><Translate>Ha ocurrido un error al crear la cita.</Translate></p> : <p><Translate>Ha ocurrido un error al modificar la cita.</Translate></p>,
                        openSnackbar:true
                    },reactTranslateChangeLanguage(this.props.user.language));
                }
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .then(() => {
                this.getAppointments();
            })
            .catch(function(e) {
                console.log(e);
            });
    }

    openRegister(){
        this.setState({
            open:false,
            openCash:true,
            appointmentIsFinished:null
        },reactTranslateChangeLanguage(this.props.user.language))
    }

    openExpress(){
        //save appointment express
        var validated=true;
        const { newCustomer, allServices }=this.state;
        const {services,employee,customer} = this.state.appointment;
        //Getting dresser id
        var dresser = this.state.dressers.filter(function(value){
            return value.name===employee
        })
        dresser = dresser[0];

        //Validating customer
        var selectedCustomer = this.state.allCustomers.filter(function(value){
            return value.phoneNumber===customer
        })
        selectedCustomer = selectedCustomer[0];
        if(!selectedCustomer){
            selectedCustomer=null;
        }else{
            selectedCustomer=selectedCustomer._id;
        }

        if(!selectedCustomer && !newCustomer && this.state.appointment.anonymousCustomer === false){
            this.setState({customerError:true})
            validated = false;
        }

        //Validating Services
        if(services && services.length){

            var selectedServicesArray = [];
            services.map((value)=>{
                let service = this.state.allServices.filter((value2)=>{
                    return value2.name===value;
                });
                selectedServicesArray.push(service[0]._id)
            });
        }else{
            this.setState({newServices: false,servicesError: true}, reactTranslateChangeLanguage(this.props.user.language))
            validated = false;
        }

        var appointment={
            "employee":dresser._id,
            "customer":selectedCustomer,
            "services":selectedServicesArray,
            "start": new Date(),
            "end":new Date(),
            "shop":this.props.salon,
            "source":"Shop",
            "status":"Confirmado",
            "observaciones":this.state.appointment.observaciones,
            "proposedServicesPriceSentToClient":this.state.appointment.proposedServicesPriceSentToClient,
            "anonymousCustomer":this.state.appointment.anonymousCustomer,
            "images":this.state.appointment.images
        };
        if(validated){
            if(newCustomer){
                this.saveNewCustomer(appointment);
            }else{
                this.saveNewAppointment(appointment);
            }
            this.setState({
                open:false,
                openCash:true,
                appointmentIsFinished:null
            }, reactTranslateChangeLanguage(this.props.user.language))
        }
    }

    appointmentIsFinished(e){
        if(e==='Citado'||e==='Cancelado') {
            this.getAppointments();
            this.setState({openCash:false});
        }
        this.setState({appointmentIsFinished:e})
    }

    selectionsRenderer = (values, hintText, name) => {
        const { isFetchingCustomers, isFetchingServices, isFetchingEmployees } = this.state

        switch (name) {
            case 'customer':
                if (isFetchingCustomers) {
                    return <div>
                        <CircularProgress size={20} style={{marginRight: 10}}/>
                        {hintText}
                    </div>
                }
                break
            case 'service':
                if (isFetchingServices) {
                    return <div>
                        <CircularProgress size={20} style={{marginRight: 10}}/>
                        {hintText}
                    </div>
                }
                break
            case 'employee':
                if (isFetchingEmployees) {
                    return <div>
                        <CircularProgress size={20} style={{marginRight: 10}}/>
                        {hintText}
                    </div>
                }
                break
            default:
        }

        if (!values) return hintText
        const { value, label } = values
        if (Array.isArray(values)) {
            return values.length
                ? values.map(({ value, label }) => label || value).join(', ')
                : hintText
        }
        else if (label || value) return label || value
        else return hintText
    }

    selectionsRenderer2 = (services) => {
        switch (services.length) {
            case 0:
                return '';
            case 1:
                return <p>{services.length}<Translate> servicio seleccionado</Translate></p>;
            default:
                return <p>{services.length}<Translate> servicios seleccionados</Translate></p>;
        }
    };

    deleteAppointment(){
        let body={
            status:'Eliminado'
        };
        db('put','/api/appointments/'+this.state.appointment._id,this.props.token,body)
            .then((response) =>{
                if(response) {
                    this.setState({
                        open: false,
                        snackMessage: <p><Translate>Cita eliminada correctamente.</Translate></p>,
                        openSnackbar: true
                    });
                }
                else {
                    this.setState({
                        open:false,
                        snackMessage: <p><Translate>Ha ocurrido un error al eliminar la cita.</Translate></p>,
                        openSnackbar:true
                    });
                }
                this.getAppointments();
            })
            .then(() => reactTranslateChangeLanguage(this.props.user.language))
            .catch(function(err) {
                console.log(err);
            });
    }

    onUpload = (src) => {
        var body={
            id:this.state.appointment._id,
            photoUrl:src
        };
        db('post','/api/appointments/image/',this.props.token,body).then((response)=>{
            this.setState({appointment:{...this.state.appointment,images:response.appointment.images}}, reactTranslateChangeLanguage(this.props.user.language))
        })
    }


    setEndTime(services){
        const {maxEventDate} = this.state

        var minutes=0,service;
        services.map((v)=>{
            service = this.state.allServices.filter((w)=>{
                return w.name===v;
            });
            minutes+=service[0].duration;
        });
        var endTime = new Date(this.state.formIniTime);
        endTime.setMinutes(endTime.getMinutes() + minutes);

        let endTimediff = this.getMinutesDiff(maxEventDate,endTime);
        if(endTimediff<0){
            endTime=new Date(maxEventDate);
            var diffTime = Math.abs(endTimediff)
            this.setState({endTimeError:(<div style={{color:'orange',fontSize:12, maxWidth:320}}><Translate>Se va a comprimir la duración de la cita en </Translate>{Math.round(diffTime)}<Translate> minutos.</Translate></div>)})
        }
        return endTime;
    }

    handleCustomerFilter = (customer) => {
        var {appointments,serviceFilter,employeeFilter} = this.state;
        this.setState({ customerFilter:customer.value })
        this.setState({ events:parseAndFilterEvents(appointments,serviceFilter,customer.value,employeeFilter) })
    }
    handleEmployeeFilter = (employee) => {
        var {appointments,customerFilter,serviceFilter} = this.state;
        this.setState({ employeeFilter:employee.value,events:parseAndFilterEvents(appointments,serviceFilter,customerFilter,employee.value) })
    }
    handleServiceFilter = (services) => {
        var {appointments,customerFilter,employeeFilter} = this.state;
        this.setState({ serviceFilter:multiselectValueToArray(services),events:parseAndFilterEvents(appointments,multiselectValueToArray(services),customerFilter,employeeFilter) })
    }

    toSelectValue(value){
        var result;
        if(typeof (value)=='undefined' || value === null){
            if(this.state.appointment.customer){
                result = {value: this.state.appointment.customer, label: this.state.appointment.customerName};
            }else return [];
        }
        else{
            result = value
        }
        return result
    }

    handleCustomerSelection = (customer) => {
        if(customer)this.setState({customerError:false})
        this.setState({
            selectedCustomer: customer,
            appointment:{...this.state.appointment,customer:customer.value
            }
        })
        this.getServices(customer.value, false);
    }

    handleEmployeeSelection = (employee) => {
        this.setState({ appointment:{...this.state.appointment,employee:employee.value }})
    }

    handleChangeFormDate = (e, value)=>{
        this.setState({ formDate: value })
        this.getDressers(value)
    }

    handleServicesSelection = (service) => {
        if(service.value){
            var {allServices} = this.state;
            var value = 0;
            var servicesDetails = true;
            var services = this.state.appointment && this.state.appointment.services ? this.state.appointment.services : [];
            services.push(service.value);
            this.setState({servicesError:false, newServices:true})
            var endTime = this.setEndTime(services);
            var servicesList = [];
            services.forEach(function (service1) {
                let service = allServices.filter((srv)=>{
                    return service1===srv.name
                })
                servicesList.push(service[0]);
                value = value + (service[0].lastPrice ? service[0].lastPrice : service[0].price);
            });
            if(services.length > 0 && servicesList[0].days){
                var days = servicesList[0].days;
                var offset = Moment().utcOffset();
                var lastDate = servicesList[0].lastDate;
                var lastDateFormatted = Moment.utc(lastDate).utcOffset(offset).format('YYYY-MM-DD');
            }
            else
                servicesDetails = false;
            this.setState({
                servicePrice: value,
                appointment:{...this.state.appointment,services:services},
                formEndTime:endTime,
                days:days,
                lastDate:lastDateFormatted,
                servicesDetails: servicesDetails
            })
        }
    }

    tabChange = (tab) => {
        this.setState({cashTab:tab})
    }

    handleRequestDelete = (index) => {
        this.setState({
            appointment: { ...this.state.appointment, services: this.state.appointment.services.filter((_, i) => i !== index) }
        });
    };

    deleteImage(src, a){
        this.setState({
            appointment: { ...this.state.appointment, images: this.state.appointment.images.filter((_, i) => i !== a) }
        });
    }

    handleActionClick(){
        this.setState({ isRedirect: true })
    };

    render(){
        var now = new Date();
        var dateTime = this.state.formIniTime == null ? new Date(): this.state.formIniTime;
        var isToday = ((now.getDate() === dateTime.getDate()) && (now.getMonth() === dateTime.getMonth()) && (now.getFullYear() === dateTime.getFullYear()));

        var phonePrefix = this.state.customer ? this.state.customer.phonePrefix : '';
        var phoneNumber = this.state.customer ? this.state.customer.phoneNumber : '';

        const cliente = typeof (this.state.appointment.customer) !== 'undefined' && this.state.appointment.customer !== null ? this.state.appointment.customerName : 'cliente desconocido';
        const contacto = typeof (this.state.appointment.customer) !== 'undefined' && this.state.appointment.customer !== null ? this.state.appointment.customer : 'contacto desconocido';
        const appointmentServices = this.state.appointment.services ? this.state.appointment.services : null;
        if(this.state.appointment.services){
            var listServices = appointmentServices.map((service) =>
                <li>{service}</li>
            );
        }

        var labelAutomaticAppointments = <Translate>Mostrar citas automáticas</Translate>;
        var labelCobrar = <Translate>Cobrar</Translate>;
        var labelCancelar = <Translate>Cancelar</Translate>;
        var labelEliminar = <Translate>Eliminar</Translate>;
        var labelConfirmar = <Translate>Confirmar</Translate>;
        var labelCobroExpress = <Translate>Cobro Express</Translate>;
        var labelCancelarReserva = <Translate>Cancelar Reserva</Translate>;
        var labelAceptarPago = <Translate>Aceptar Pago</Translate>;
        var labelSalir = <Translate>Salir</Translate>;
        var labelCliente= <Translate>Cliente</Translate>;
        var labelAnonymous= <Translate>Anonymous</Translate>;
        var labelEstilista= <Translate>Estilista</Translate>;
        var labelHintEstilista= <Translate>Selecciona un estilista...</Translate>;
        var labelHoraInicio= <Translate>H. inicio</Translate>;
        var labelHoraFinal= <Translate>H. final</Translate>;
        var labelServicios= <Translate>Servicios</Translate>;
        var labelHintServicios= <Translate>Selecciona servicios...</Translate>;
        var labelDias= <Translate>Días</Translate>;
        var labelUltimaFecha= <Translate>Última fecha</Translate>;
        var labelPrecioServicios= <Translate>Precio de los servicios</Translate>;
        var labelPresupuesto= <Translate>Notificar presupuesto</Translate>;
        var labelObservaciones= <Translate>Observaciones</Translate>;
        var labelHintObservaciones= <Translate>¿Alguna observación?</Translate>;
        var labelNuevoCliente= <Translate>Nuevo Cliente</Translate>;
        var labelPrefijo= <Translate>Prefijo</Translate>;
        var labelTelefono= <Translate>Teléfono*</Translate>;
        var labelCorreo= <Translate>Correo</Translate>;
        var labelNombre= <Translate>Nombre</Translate>;
        var labelApellido= <Translate>Apellido</Translate>;
        var labelPuntoVenta= <p><Translate>Punto de Venta</Translate></p>
        var labelPosterior= <p><Translate>La hora de fin de cita es posterior a la hora de cierre. ¿Continuar?</Translate></p>;
        var labelEliminarCita= <p><Translate>Vas a eliminar la cita. ¿Continuar?</Translate></p>
        
        var appointmentCount = this.state.appointmentUnconfirmed ? this.state.appointmentUnconfirmed : 0
        let customerImagesPreviewUrl = this.state.appointment.images;
        let $customerImagesPreview = null;
        if(typeof (customerImagesPreviewUrl) !== 'undefined' && customerImagesPreviewUrl.length > 0){
                const listOfImages = customerImagesPreviewUrl.map((value,a) =>
                    <div key={a} style={{float:'left', marginBottom:10, marginLeft:-5}}>
                        <IconButton
                            style={{left:20,backgroundColor:'rgba(256,256,256,0.8)', width:20, height:20, padding:0, top:-6}}
                            onTouchTap={()=>this.deleteImage(value, a)}
                        >
                            <Clear id="clearButton"/>
                        </IconButton><img
                        alt=""
                        key={a}
                        src={value}
                        onClick={()=> window.open(value, "_blank")}
                        style={
                            {maxWidth:50, marginRight:2,position:'relative',cursor:'pointer'}
                        }/>
                    </div>
                );
                $customerImagesPreview = (
                    <div style={{width:300}}>
                        {listOfImages}
                    </div>
                )
        }
        else{
            $customerImagesPreview = (
                <div style={{clear:'both'}}>
                    <p><Translate>No hay imágenes disponibles.</Translate></p>
                </div>
            )

        }

        var snackMessage = this.state.snackMessage ? this.state.snackMessage : ""
        var openSnackbar = this.state.openSnackbar;
        var openActionSnackbar = this.state.openActionSnackbar;
        var snackActionMessage = this.state.snackActionMessage ? this.state.snackActionMessage : ""


        const {customer,services,employee, proposedServicesPriceSentToClient,status} = this.state.appointment;
        const {
            appointments,
            customers,
            appointmentIsFinished,
            newCustomer,
            newServices,
            formIniTime,
            formEndTime,
            calendarDay,
            endTimeError,
            servicesError,
            customerError,
            formIniTimeError,
            cancelOrPay,
            scheduled,
            appointment,
            cashTab,
            opening,
            servicesDetails,
            holidays,
            phoneNumbererror,
            isRedirect
            } = this.state;

        let today = calendarDay.getDay();

        if(opening){
            let m = opening[today] ? opening[today].morning : null
            let e = opening[today] ? opening[today].evening : null
            this.morningClosing = m ? m.end : null
            this.eveningClosing = e ? e.end : null
            this.eveningStart = e ? e.start : null
            this.morningStart = m ? m.start : null
            this.slots=m ? Math.round((e.end/100-m.start/100) * 2) : 0;
        }

        const servicesDivs = this.state.services;
        const saveBtnText = this.state.newForm ? <Translate>Guardar</Translate> : <Translate>Modificar</Translate>;
        const dialogTitle = this.state.newForm ? <p><Translate>Añadir nueva Reserva</Translate></p> : <p><Translate>Modificar Reserva</Translate></p>
        const statusColor = this.state.appointment.status === 'Pagado' ? grey700 : '';
        const statusTitle = this.state.appointment.status;
        const sourceTitle = this.state.appointment.source;

        const divTitle =(
            <div>
                <div style={{display:'inline-block'}}>
                    {dialogTitle}
                </div>
                <div style={{display:'inline-block', float:'right', marginTop: -7}}>
                    <Chip
                        style={styles.chip}
                        backgroundColor={statusColor}
                        labelStyle={{paddingLeft:10,paddingRight:10}}
                    >
                        <p style={{color:this.state.appointment.status === 'Pagado' ? '#ffffff' : '#000000', display:'inline', textAlign:'center', opacity:0.87, fontSize:'12'}}>{statusTitle}</p>
                    </Chip>
                    <Chip
                        style={styles.chip}
                        backgroundColor={teal700}
                        labelStyle={{paddingLeft:10,paddingRight:10}}
                    >
                        <p style={{color:'#ffffff', display:'inline', textAlign:'center', opacity:0.87, fontSize:'12'}}>{sourceTitle}</p>
                    </Chip>
                </div>
            </div>
        );

        var {selectedCustomer} = this.state;

        var $chips = null;
        if(services && services.length){
            $chips = services.map((service,i) =>
                    <Chip
                        key={i}
                        onRequestDelete={this.handleRequestDelete.bind(this, i)}
                        style={{ display:'inline-flex', marginTop:5, marginLeft:2 }}
                    >
                        {service}
                    </Chip>
            );
        }

        var serviceFilterOptions,customerFilterOptions;
        const allOption = (<div key={123} value={'Todos'}>Todos</div>);
        if(servicesDivs && servicesDivs.length>0){
            if(servicesDivs[servicesDivs.length - 1].key!=='123'){
                serviceFilterOptions = _arrayCopy(servicesDivs);
                serviceFilterOptions.push(allOption);
            }else{
                serviceFilterOptions = _arrayCopy(servicesDivs)
            }
        }else{
            serviceFilterOptions = [allOption]
        }

        if(customers && customers.length>0){
            if(customers[customers.length - 1].key!=='123'){
                customerFilterOptions = _arrayCopy(customers);
                customerFilterOptions.push(allOption)
            }else{
                customerFilterOptions = _arrayCopy(customers)
            }
        }else{
            customerFilterOptions = [allOption]
        }

        let actions = [
            <FlatButton
                label={labelCobrar}
                disabled={this.state.appointment.status === 'Pendiente'}
                primary={true}
                onTouchTap={this.openRegister.bind(this)}
            />,
            <FlatButton
                label={labelCancelar}
                primary={true}
                onTouchTap={this.closeNewAppointment.bind(this)}
            />,
            <FlatButton
                label={saveBtnText}
                disabled={this.state.appointment.status === 'Pagado' || this.state.appointment.status === 'Pendiente'}
                primary={true}
                onTouchTap={this.validateData.bind(this)}
            />
        ];

        if((appointment && appointment.status==='Pagado')||!appointment._id)actions.splice(0, 1);


            if(!this.state.newForm)actions.push(
            <FlatButton
                label={labelEliminar}
                secondary={true}
                disabled={this.state.appointment.status === 'Pagado'}
                onTouchTap={this.deleteConfirmation}
            />
        );
        if(this.state.appointment.status === 'Pendiente')actions.push(
            <RaisedButton
                label={labelConfirmar}
                secondary={true}
                onTouchTap={()=>this.setState({ appointment: { ...this.state.appointment, status: 'Confirmado' }}, this.validateData() )}
            />
        );
        else if(isToday && this.state.newForm){
                actions.push(
                    <FlatButton
                        label={labelCobroExpress}
                        secondary={true}
                        onTouchTap={this.openExpress.bind(this)}
                        style={{ float: 'left'}}
                    />
                );
            }

        var registerActions = [
            <RaisedButton
                labelColor={'#ffffff'}
                label={labelCancelarReserva}
                labelPosition="before"
                backgroundColor="#c0392b"
                onTouchTap={()=>{this.setState({cancelOrPay:'cancel',cashTab:'f'})}}
                icon={<NavigationCancel color={'#ffffff'} />}
                style={style}
            />,
            <RaisedButton
                labelColor={'#ffffff'}
                label={labelAceptarPago}
                labelPosition="before"
                backgroundColor="#27ae60"
                onTouchTap={()=>{this.setState({cancelOrPay:'pay',cashTab:'f'})}}
                icon={<ActionShoppingCart color={'#ffffff'} />}
                style={style}
            />,
            <FlatButton
                label={labelCancelar}
                primary={true}
                onTouchTap={this.closeRegister.bind(this)}
                style={style}
            />
        ];

        if(cashTab==='f' && cancelOrPay==='pay'){
            registerActions.splice(0,2)
            registerActions.push(
                <RaisedButton
                    label={<Translate>Confirmar</Translate>}
                    secondary={true}
                    //disabled={this.refs.child.state.paymentMethodButton === "Mixed" && this.refs.child.state.disabledAccept}
                    fullWidth={false}
                    onTouchTap={() => this.refs.child.saveImages()}
                />
            )
        }

        if(appointmentIsFinished){
            registerActions=[
                <FlatButton
                    label={labelSalir}
                    primary={true}
                    onTouchTap={this.closeRegister.bind(this)}
                    style={style}
                />
            ]
        }

        let defaultDate = formIniTime ? {defaultDate:formIniTime} : null;

        let filteredApptms=[];
        if(appointments){
            filteredApptms = appointments.filter((v)=>{
                if (!scheduled && v.createdBy === 'Scheduler' && v.status === 'Pendiente')return false;
                return true;
            })
        }
        if (isRedirect) {console.log('Redirected to Till..')
            return (
                <Redirect to={{pathname:'/caja'}}/>
            )
        }

        return(
            <div className="calendar-container">
                <Snackbar
                    open={openActionSnackbar}
                    message={snackActionMessage}
                    action={<Translate>Abrir</Translate>}
                    bodyStyle={{ height: 'auto', lineHeight: '10px', whiteSpace: 'pre-line', paddingTop: 10, paddingBottom: 10, paddingLeft:10, paddingRight:15, width:375 }}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                    onClick={this.handleActionClick}
                />
                <Snackbar
                    open={openSnackbar}
                    message={snackMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <h1><Translate>AGENDA</Translate></h1>
                <div style={{right:10,float:'right',position:'relative',minWidth: 300, display:'inline-flex'}}>
                        <Checkbox
                            label={labelAutomaticAppointments}
                            labelPosition="left"
                            checked={scheduled}
                            onCheck={()=>this.setState({ scheduled:!scheduled}, this.forceUpdateHandler)}
                            style={styles.checkbox}
                            iconStyle={styles.checkboxIcon}
                            inputStyle={styles.checkbox}
                            labelStyle={styles.checkbox}
                        />
                    {(this.state.appointmentUnconfirmed > 0) &&
                        <div>
                            <p style={styles.unconfirmedMessageStyle}><Translate>Citas por confirmar: </Translate><p
                                style={{display:'inline'}}>{appointmentCount}</p></p>
                        </div>
                    }
                </div>
                <DayCalendar
                    selectable
                    events={filteredApptms}
                    dressers={this.state.dressers}
                    style={{height:700}}
                    culture='es'
                    defaultView='day'
                    slots={this.slots}
                    allServices={this.state.allServices}
                    allProducts={this.state.allProducts}
                    holidays={holidays}
                    cellHeight={30}
                    onSelectSlot={(slotInfo) => this.openNewAppointment(slotInfo)}
                    onSelectEvent={(event) => this.editAppointment(event)}
                    calendarDay={calendarDay}
                    min={this.morningStart ? parseInt(this.morningStart/100):null}
                    onDaySelect={(newCalendarDay)=>this.onDaySelect(newCalendarDay)}
                    morningStart={this.morningStart}
                    eveningClosing={this.eveningClosing}
                />
                {defaultDate &&
                <Dialog
                    title={divTitle}
                    titleClassName="dialog-title"
                    actions={actions}
                    modal={true}
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                    open={this.state.open}
                    >
                    <div id="automaticAppointmentTrue" className={"collapse" + (status === 'Confirmado' || status === 'Pagado' || status === 'Cancelado' || this.state.newForm === true ? ' in' : '')}>
                    <div style={{float:'left'}}>
                        <br/>
                        <div className="cliente">
                        <SuperSelectField
                            name='customer'
                            floatingLabel={labelCliente}
                            hintText=''
                            disabled={this.state.appointment.anonymousCustomer}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText, 'customer')}
                            value={this.toSelectValue(selectedCustomer)}
                            onChange={this.handleCustomerSelection}
                            checkPosition='right'
                            style={{ width: 150, display: 'inline-block' }}
                        >
                            {customers}
                        </SuperSelectField>
                        <Checkbox
                            label={labelAnonymous}
                            labelPosition="right"
                            checked={this.state.appointment.anonymousCustomer}
                            onCheck={(e, value)=>this.setState({ appointment: { ...this.state.appointment, anonymousCustomer: value, customer: null, customerName: null }, selectedCustomer: null, customerError:false},this.getServices)}
                            style={{color:'#666', width:160, display:'inline-block', marginLeft:10}}
                            iconStyle={{marginTop:10, width:24, height:24, fill:'#666'}}
                            labelStyle={{marginTop:20, color:'#666'}}
                        />
                            {customerError && <div style={{color:'red',fontSize:12}}><p><Translate>No has seleccionado ningún Cliente</Translate></p></div>}
                        </div>
                        <br/><br/>
                        <SuperSelectField
                            name='employee'
                            floatingLabel={labelEstilista}
                            hintText={labelHintEstilista}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText, 'employee')}
                            value={{value:employee}}
                            onChange={this.handleEmployeeSelection}
                            checkPosition='right'
                            style={{ width: 290 }}
                        >
                            {this.state.employees}
                        </SuperSelectField>
                        <br/>
                        <div style={{ display: 'inline-flex'}}>
                        <DatePicker
                            name="date"
                            mode="landscape"
                            {...defaultDate}
                            onChange={this.handleChangeFormDate}
                            textFieldStyle={{ width: 100 }}
                            inputStyle={{textAlign:'center'}}
                        /><br/>
                        <TimePicker
                            name="iniTimeF"
                            format="24hr"
                            hintText={labelHoraInicio}
                            value={defaultDate.defaultDate}
                            onChange={(e, value)=>this.setState({ formIniTime: value})}
                            style={styles.timepickers}
                            textFieldStyle={styles.timepickersTF}
                            inputStyle={{textAlign:'center'}}
                        /><br/>
                        <TimePicker
                            name="endTimeF"
                            format="24hr"
                            hintText={labelHoraFinal}
                            defaultTime={null}
                            value={formEndTime}
                            onChange={(e, value)=>this.setState({  formEndTime: value })}
                            style={styles.timepickers}
                            textFieldStyle={styles.timepickersTF}
                            inputStyle={{textAlign:'center'}}
                        /><br/>
                        </div>
                        {formIniTimeError && (<div style={{color:'red',fontSize:12,marginTop:5}}><Translate>Hora fuera del horario disponible</Translate></div>)}
                        {endTimeError}
                        <div style={{marginTop:10, width:300}}>
                            <Translate>Fotografías adjuntas:</Translate>
                            {$customerImagesPreview}
                        </div>
                    </div>
                    <div style={{float:'right'}}><br/><br/>
                        <SuperSelectField
                            name='services'
                            floatingLabel={labelServicios}
                            hintText={labelHintServicios}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer2(values, hintText, 'service')}
                            value={arrayToMultiselectValue(services)}
                            onChange={this.handleServicesSelection}
                            checkPosition='right'
                            style={{ width: 250 }}
                        >
                            {servicesDivs}
                        </SuperSelectField>
                        <div style={{width:250}}>
                        {$chips}
                        </div>
                        {servicesError && (<div style={{color:'red',fontSize:12}}><p><Translate>No ha seleccionado ningún servicio</Translate></p></div>)}
                        {servicesDetails &&
                            <div>
                                <TextField
                                    floatingLabelText={labelDias}
                                    name="days"
                                    value={this.state.days}
                                    onChange={(e, value)=>this.setState({ days: value})}
                                    style={{ width: 125 }}
                                />
                                <TextField
                                    floatingLabelText={labelUltimaFecha}
                                    name="lastDate"
                                    value={this.state.lastDate}
                                    onChange={(e, value)=>this.setState({ lastDate: value})}
                                    style={{ width: 125 }}
                                /></div>
                        }
                        <br/>
                        <TextField
                            floatingLabelText={labelPrecioServicios}
                            name="servicePrice"
                            value={this.state.servicePrice}
                            onChange={(e, value)=>this.setState({ servicePrice: value})}
                            style={{ width: 250 }}
                        />
                        <br/>
                        <Checkbox
                            label={labelPresupuesto}
                            labelPosition="left"
                            checked={proposedServicesPriceSentToClient ? true : false}
                            onCheck={(e, value)=>this.setState({ appointment: { ...this.state.appointment, proposedServicesPriceSentToClient: value } })}
                            disabled={proposedServicesPriceSentToClient}
                            style={styles.checkboxPresupuesto}
                            iconStyle={styles.checkboxPresupuesto}
                            inputStyle={styles.checkboxPresupuesto}
                            labelStyle={styles.checkboxPresupuesto}
                        />
                        <TextField
                            fullWidth={true}
                            floatingLabelText={labelObservaciones}
                            defaultValue={this.state.appointment.observaciones}
                            onChange={(e, value)=>this.setState({ appointment: { ...this.state.appointment, observaciones: value } })}
                            multiLine={true}
                            hintText={labelHintObservaciones}
                        /><br/><br/>
                        <RaisedButton
                            label={labelNuevoCliente}
                            onTouchTap={()=>{this.setState({newCustomer:true})}}
                            icon={<SocialPersonAdd color={'#666'} />}
                            style={{width:250}}
                        />
                        {newCustomer &&
                            <div>
                             <div style={{float:'right', display:'flex'}}>
                                <SelectField
                                    floatingLabelText={labelPrefijo}
                                    value={phonePrefix}
                                    onChange={(e, i, value)=>this.setState({ customer: { ...this.state.customer, phonePrefix: value } })}
                                    autoWidth={true}
                                    style={{width:128, display:'inline-block'}}
                                >
                                    <MenuItem primaryText="+34" value={"+34"} leftIcon={<img src={SpainLogo} className="Spain-logo" alt="Spain-logo" />} />
                                    <MenuItem primaryText="+55" value={"+55"} leftIcon={<img src={BrasilLogo} className="Brasil-logo" alt="Brasil-logo" />} />
                                    <MenuItem primaryText="+44" value={"+44"} leftIcon={<img src={UKLogo} className="UK-logo" alt="UK-logo" />} />
                                </SelectField>
                                 <TextField
                                     floatingLabelText={labelTelefono}
                                     defaultValue={phoneNumber}
                                     name="phoneNumber"
                                     type="number"
                                     onChange={(e, value)=>this.setState({ customer: { ...this.state.customer, phoneNumber: value } })}
                                     style={{width:123, display:'inline-block', marginLeft:5}}
                                 />
                             </div>
                                {phoneNumbererror}
                                 <br/>
                                <TextField
                                    floatingLabelText={labelCorreo}
                                    name="email"
                                    onChange={(e, value)=>this.setState({ customer: { ...this.state.customer, email: value } })}
                                /><br/>
                                <TextField
                                    floatingLabelText={labelNombre}
                                    name="name"
                                    onChange={(e, value)=>this.setState({ customer: { ...this.state.customer, name: value } })}
                                /><br/>
                                <TextField
                                    floatingLabelText={labelApellido}
                                    name="surname"
                                    onChange={(e, value)=>this.setState({ customer: { ...this.state.customer, surname: value } })}
                                /><br/>
                            </div>
                        }
                    </div>
                    </div>
                    <div id="automaticAppointmentFalse" className={"collapse" + (this.state.appointment.status === 'Pendiente' ? ' in' : '')}>
                        <h3><Translate>Es necesario confirmar la cita</Translate></h3>
                        <p><Translate>Datos del cliente:</Translate></p>
                        <ul>
                            <li><Translate>Cliente: </Translate>{cliente}</li>
                            <li><Translate>Contacto: </Translate>{contacto}</li>
                            <li><Translate>Servicios:</Translate></li>
                            <ul>
                                {listServices}
                            </ul>
                        </ul>
                    </div>
                </Dialog>
                }
                <Dialog
                    title={labelPuntoVenta}
                    titleClassName="dialog-title"
                    actions={registerActions}
                    modal={true}
                    contentStyle={{width:800,height:600}}
                    bodyStyle={{padding:0}}
                    autoScrollBodyContent={true}
                    open={this.state.openCash}
                >
                    <CashRegister
                        appointment={this.state.appointment}
                        allCustomers={this.state.allCustomers}
                        allEmployees={this.state.dressers}
                        allServices={this.state.allServices}
                        allProducts={this.state.allProducts}
                        cashTab={cashTab}
                        onTabChange={this.tabChange}
                        cancelOrPay={cancelOrPay}
                        token={this.props.token}
                        salon={this.props.salon}
                        user={this.props.user}
                        onUpload={this.onUpload}
                        finished={this.appointmentIsFinished.bind(this)}
                        deleteImage={this.deleteImage.bind(this)}
                        ref="child"
                    />
                </Dialog>
                <Dialog
                    title={labelPosterior}
                    actions={[
                          <FlatButton
                            label={labelCancelar}
                            primary={true}
                            onClick={() => {this.setState({openClosingTimeConfirmation: false,open:true})}}
                          />,
                          <FlatButton
                            label={labelConfirmar}
                            primary={true}
                            onClick={()=>{this.setState({eventClosingConfirmed:true,openClosingTimeConfirmation:false},this.validateData)}}
                          />,
                        ]}
                    modal={false}
                    open={this.state.openClosingTimeConfirmation}
                    onRequestClose={() => {this.setState({openClosingTimeConfirmation: false,open:true})}}
                >
                </Dialog>
                <Dialog
                    title={labelEliminarCita}
                    actions={[
                          <FlatButton
                            label={labelCancelar}
                            primary={true}
                            onClick={() => {this.setState({openConfirmDeleting: false,open:true})}}
                          />,
                          <FlatButton
                            label={labelConfirmar}
                            primary={true}
                            onClick={()=>{this.setState({openConfirmDeleting:false},this.deleteAppointment.bind(this))}}
                          />,
                        ]}
                    modal={false}
                    open={this.state.openConfirmDeleting}
                    onRequestClose={() => {this.setState({openConfirmDeleting: false,open:true})}}
                >
                </Dialog>
            </div>
        )
    }

    //TODO: Get Dressers at login and save in session
    componentDidMount(){
        console.log("componentDidMount Calendar")
        reactTranslateChangeLanguage(this.props.user.language)
        this.getDressers();
        this.getCustomers();
        this.getProducts();
        this.getServices(null, true);
        this.getAppointments();
        this.getClosing();
        this.verifyTill();
        this.interval= setInterval(()=>this.getAppointments(), 60000);
    }

    componentDidUpdate() {
        setTimeout(() => reactTranslateChangeLanguage(this.props.user.language), 50)
    }

    componentWillUnmount(){
        console.log("componentWillUnmount Calendar")
        reactTranslateChangeLanguage(this.props.user.language)
        clearInterval(this.interval);
        clearTimeout(this.timer);
    }

    setInitialData(responseData) {
        console.log("setInitialData Calendar")
        var row;
        let myEventArray=this.state.appointments;
        if(responseData){
            myEventArray=[];
            responseData.forEach(function(value, index, array){
                row={
                    'title': value.services[0].description,
                    'start': value.appointment.start,
                    'end': value.appointment.end,
                };
                myEventArray.push(row);
            });
        }

        this.setState({appointments:myEventArray})
    }
}

export default Calendar
