﻿import React, {Component} from 'react';
import { Link,Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import Drawer from 'material-ui/Drawer';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import {spacing, typography, zIndex} from 'material-ui/styles';
import {cyan500} from 'material-ui/styles/colors';

import ActionDateRange from 'material-ui/svg-icons/action/date-range';
import ActionHome from 'material-ui/svg-icons/action/home';
import ActionShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import Stars from 'material-ui/svg-icons/action/stars';
import ActionShoppingBasket from 'material-ui/svg-icons/action/shopping-basket';
import SocialGroup from 'material-ui/svg-icons/social/group';
import ContentContentCut from 'material-ui/svg-icons/content/content-cut';
import ContentMail from 'material-ui/svg-icons/content/mail';
import ImageTagFaces from 'material-ui/svg-icons/image/tag-faces';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import RaisedButton from 'material-ui/RaisedButton';
import ActionPowerSettingsNew from 'material-ui/svg-icons/action/power-settings-new';
import IconButton from 'material-ui/IconButton';
import Menu from 'material-ui/svg-icons/navigation/menu';
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'

const SelectableList = makeSelectable(List);

const styles = {
    logo: {
        cursor: 'pointer',
        fontSize: 24,
        color: typography.textFullWhite,
        lineHeight: `${spacing.desktopKeylineIncrement}px`,
        fontWeight: typography.fontWeightLight,
        backgroundColor: cyan500,
        paddingLeft: spacing.desktopGutter,
        marginBottom: 8,
    },
    version: {
        paddingLeft: spacing.desktopGutterLess,
        fontSize: 16,
        padding:5
    },
    listItem:{
        color:'white',
        fontFamily:'HelveticaTh',
        fontSize:15
    },
    iconColor:'white',
    iconStyle:{
        margin:5
    },
    button:{
        margin:10,
        position:'absolute',
        bottom:0
    },
    innerDiv:{
        padding:'10px 5px 15px 45px'
    }
};

class SASideMenu extends Component {
    static propTypes = {
        docked: PropTypes.bool.isRequired,
        location: PropTypes.object.isRequired,
        onChangeList: PropTypes.func.isRequired,
        onRequestChangeNavDrawer: PropTypes.func.isRequired,
        open: PropTypes.bool.isRequired,
        style: PropTypes.object,
    };

    static contextTypes = {
        muiTheme: PropTypes.object.isRequired,
        //router: PropTypes.object.isRequired,
    };

    state = {
        muiVersions: [],
    };

    firstNonPreReleaseVersion() {
        let version;
        for (let i = 0; i < this.state.muiVersions.length; i++) {
            version = this.state.muiVersions[i];
            // If the version doesn't contain '-' and isn't 'HEAD'
            if (!/-/.test(version) && version !== 'HEAD') {
                break;
            }
        }
        return version;
    }

    handleVersionChange = (event, index, value) => {
        if (value === this.firstNonPreReleaseVersion()) {
            window.location = 'http://www.material-ui.com/';
        } else {
            window.location = `http://www.material-ui.com/${value}`;
        }
    };

    currentVersion() {
        if (window.location.hostname === 'localhost') return this.state.muiVersions[0];
        if (window.location.pathname === '/') {
            return this.firstNonPreReleaseVersion();
        } else {
            return window.location.pathname.replace(/\//g, '');
        }
    }

    handleRequestChangeLink = (event, value) => {
        window.location = value;
    };

    handleTouchTapHeader = () => {
        this.context.router.push('/');
        this.props.onRequestChangeNavDrawer(false);
    };

    render() {

        var menuText = this.state.open ? "Cerrar menú" : "Abrir menú";


        const {
            location,
            docked,
            onRequestChangeNavDrawer,
            onChangeList,
            open,
            style,
            } = this.props;

        let shopName = 'Super Admin';
        let shopAddress = '';

        var labelSalons = <Translate>Salones</Translate>
        var labelBrands = <Translate>Marcas</Translate>
        var labelProducts = <Translate>Productos</Translate>
        var labelServices = <Translate>Servicios</Translate>
        var labelOrders = <Translate>Pedidos</Translate>
        var labelCampaigns = <Translate>Campañas</Translate>
        var labelCustomers = <Translate>Clientes</Translate>
        var labelStatistics = <Translate>Estadísticas</Translate>
        var labelExit = <Translate>SALIR</Translate>

        return (
            <div>
                <IconButton
                    onClick={this.props.handleToggle}
                    tooltip={menuText}
                    tooltipPosition="top-right"
                    style={{zIndex: 1301}}
                >
                    <Menu />
                </IconButton>
                <Drawer
                    width={150}
                    open={this.props.openMenu}
                >
            <div
                className='side-menu'
                style={{
                    height:'100%',
                    float:'left',
                    backgroundColor: '#343B45',
                    color: 'white',
                    position:'fixed',
                    top:0,
                    overflow:'auto',
                    overflowX:'hidden'
               }}
            >
                <div>
                    <img className="salon-logo" src={'http://test.heliocor.com/magnifique/img/urb16.png'}/>
                </div>
                <h1>{shopName.toUpperCase()}</h1>
                <h4>{shopAddress}</h4>
                <SelectableList
                    value={location.pathname}
                    onChange={onChangeList}
                >
                    <ListItem
                        primaryText={labelSalons}
                        style={styles.listItem}
                        leftIcon={<ActionHome color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/salones"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                    <ListItem
                        primaryText={labelProducts}
                        style={styles.listItem}
                        leftIcon={<ActionShoppingCart color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/productos"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                    <ListItem
                        primaryText={labelBrands}
                        style={styles.listItem}
                        leftIcon={<Stars color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/marcas"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                    <ListItem
                        primaryText={labelServices}
                        style={styles.listItem}
                        leftIcon={<ContentContentCut color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/servicios"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                    <ListItem
                        primaryText={labelOrders}
                        style={styles.listItem}
                        leftIcon={<ActionShoppingBasket color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/pedidos"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                    <ListItem
                        primaryText={labelCampaigns}
                        style={styles.listItem}
                        leftIcon={<ContentMail color={styles.iconColor} style={styles.iconStyle}/>}
                        containerElement={<Link to="/sadmin/campañas"/>}
                        innerDivStyle={styles.innerDiv}
                    />
                </SelectableList>
            </div>
                </Drawer>
            </div>
        );
    }

    componentDidMount() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
    }
    componentDidUpdate() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
    }

}

export default SASideMenu;