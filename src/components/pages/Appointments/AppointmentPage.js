import React, { Component } from 'react';
import TrendingNews from './TrendingNews'
import Calendar from './Calendar'
import './style.css';

class AppointmentPage extends Component{

    render(){
        return(
            <div className="main-container">
                <TrendingNews
                    token={this.props.token}
                    salon={this.props.salon}
                />
                <Calendar
                    handleChangeList={this.props.handleChangeList}
                    token={this.props.token}
                    salon={this.props.salon}
                    user={this.props.user}
                />
            </div>
        )
    }
}

export default AppointmentPage;