import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { TranslateProvider } from 'translate-components'
import translations from './translations.json'


ReactDOM.render(
    <TranslateProvider debugMode={false} translations={translations} defaultLanguage={'es'}>
        <App />
    </TranslateProvider>,
  document.getElementById('root')
);

