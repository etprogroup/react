import React, { Component } from 'react';
import SuperSelectField from '../../SuperSelectField';
import LinearProgress from 'material-ui/LinearProgress';
import ReactDOM from 'react-dom'
import ActionSearch from 'material-ui/svg-icons/action/search';
import DataTables from 'material-ui-datatables';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from '../../../../node_modules/material-ui/svg-icons/content/add';
import ContentRemove from '../../../../node_modules/material-ui/svg-icons/content/remove';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import {DOMAIN} from '../../../config'
import moment from 'moment';
import SelectField from 'material-ui/SelectField';
import Subheader from 'material-ui/Subheader';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import {blue500} from 'material-ui/styles/colors';
import EditorInsertChart from 'material-ui/svg-icons/editor/insert-chart';
import {db} from '../../db'
import Snackbar from 'material-ui/Snackbar';

const styles = {
    customWidth: {
        width: 150,
    },
    button: {
        marginTop: 12
    },
    slide: {
        padding: 10,
    },
    table:{
        width: 120
    },
    tableInput:{
        textAlign: 'center'
    },
    tdStyle:{
        width:120,
        paddingLeft:15,
        paddingRight:15
    },
    checkbox:{
        color:'#666'
    },
    datepickers:{
        display:'inline-block',
        marginTop:5
    },
    datepickersTF:{
        width:120
    }
};

const ORDERS_TABLE_COLUMNS = [
    {
        key: 'customerName',
        label: 'Cliente',
        style: {width: '20%'},
    }, {
        key: 'orderType',
        label: 'Orden',
        style:{width: '18%'}
    }, {
        key: 'statusText',
        label: 'Estado',
        style:{width: '10%'}
    },{
        key: 'created',
        label: 'Fecha de creación',
        style:{width: '18%'},
        render: (DateToFormat) => {
            return moment(DateToFormat).format("DD/MM/YYYY hh:mm:ss");
        }
    }, {
        key: 'shopName',
        label: 'Tienda',
        style:{width: '15%'},
    }, {
        key: 'totalAmount',
        label: 'Precio',
        style:{width: '10%'},
        render: (totalAmount) => {
            var price = totalAmount ? totalAmount/100 : 0;
            return price.toFixed(2).toString().replace('.',',')+' €';
        }
    }, {
        key: 'lastModified',
        label: 'Última modificación',
        style:{width: '18%'},
        render: (DateToFormat) => {
            return moment(DateToFormat).format("DD/MM/YYYY hh:mm:ss");
        }
    }
];


class SAOrdersPage extends Component{

    constructor(props){
        super(props);

        this.state = {
            date_filter: "total",
            page: 1,
            rowSize: 5,
            rowCount: 0,
            rowSizeList: [5,10,20,30],
            orders:[],
            order:{},
            customers:[],
            customer:{},
            products:[],
            product:{},
            openNew:false,
            openModify:false,
            value: 5,
            shop:this.props.salon,
            addStatus:false,
            openSnackbar: false
        };

        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.getOrders = this.getOrders.bind(this);
    }

    handleCellClick = (y,x,row) => {
        var status = Number(row.status);
        this.setState({
            openModify: true,
            status:status,
            addStatus:false,
            comments: "",
            paymentmethod: "",
            order:{...row, _id:row._id}
        });
    };

    //functions pagination
    handlePreviousPageClick() {
        var currentPage = this.state.page
        var array = this.state.allOrders;
        this.setState({
            page: currentPage-1,
            orders: array.slice(this.state.rowSize * (currentPage - 2), this.state.rowSize * (currentPage-1))
        });
    }

    handleNextPageClick() {
        var currentPage = this.state.page;
        var array = this.state.allOrders;
        this.setState({
            page: currentPage + 1,
            orders: array.slice(this.state.rowSize * (currentPage), this.state.rowSize * (currentPage + 1))
        });
    }

    onRowSizeChange =(a) => {
        var array = this.state.allOrders;
        this.setState({
            rowSize: this.state.rowSizeList[a],
            page: 1,
            orders: array.slice(this.state.rowSizeList[a] * (0), this.state.rowSizeList[a] * (1))
        });
    }

    handleClose = () => {
        this.setState({openNew: false, openModify: false});
    };

    handleRequestCloseSnackbar = () => {
        this.setState({
            openSnackbar: false,
        });
    };

    getOrders(){
        this.setState({
            loading: true
        });
        var { queryDateIni, queryDateEnd, queryShops, queryStatus, queryOrderType, queryBrands} = this.state
        var query = "";
        if (queryOrderType) {
            query = '?orderType=' + queryOrderType;
        }
        if(queryDateIni && queryDateEnd){
            query = (query === "") ? ('?dateIni=' + queryDateIni + '&dateEnd=' + queryDateEnd) : (query + '&dateIni=' + queryDateIni + '&dateEnd=' + queryDateEnd);
        }
        if (queryShops && queryShops.length) {
            query = (query === "") ? '?shops=' + queryShops : query + '&shops=' + queryShops;
        }
        if (queryBrands && queryBrands.length) {
            query = (query === "") ? '?brand=' + queryBrands : query + '&brand=' + queryBrands;
        }
        if(queryStatus){
            query = (query === "") ? '?status=' + queryStatus : query + '&status=' + queryStatus;
        }
        fetch(
            DOMAIN+'/api/orders/'+ query, {
                method: 'get',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer '+this.props.token
                }
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                responseData.map((order) => {
                    order.status = order.orderStatusChange && order.orderStatusChange.length ? order.orderStatusChange[order.orderStatusChange.length - 1].status : ""
                    order.statusText = this.getStatusText(order.status);
                    order.customerName = order.customer ? order.customer.surname ? order.customer.name + ' ' + order.customer.surname : order.customer.name : ""
                    order.shopName = order.shop ? order.shop.settings.name : ""
                });
                var slice = responseData.length > this.state.rowSize ? responseData.slice(this.state.rowSize * (this.state.page - 1), this.state.rowSize * (this.state.page)) : responseData;
                this.setState({
                    orders:slice,
                    allOrders:responseData,
                    rowCount:responseData.length,
                    loading: false
                });
            })
            .catch(function(err) {
                console.log("error");
                console.log(err);
                this.setState({
                    loading: false
                });
            });
    }

    multiselectValueToArray(value){
        var array=[];
        value.map(function(a){
            array.push({ "value" : a.value, "label": a.label})
        });
        return array
    }

    selectionsRenderer = (values, hintText) => {
        if (!values) return hintText
        const { value, label } = values
        if (Array.isArray(values)) {
            return values.length
                ? values.map(({ value, label }) => label || value).join(', ')
                : hintText
        }
        else if (label || value) return label || value
        else return hintText
    };

    arrayToMultiselectValue(value){
        if(typeof (value)=='undefined')return []
        var array=[];
        value.map(function(a){
            array.push({"value": a.value, "label" : a.label})
        });
        return array
    }

    handleShopsMultiSelection = (shops) => {
        var array=[];
        if(shops && shops.length > 0){
            shops.map(function(a){
                array.push(a.value)
            });
        }
        this.setState({
            selectedShops: this.multiselectValueToArray(shops),
            queryShops: array
        });
    };
    handleBrandsMultiSelection = (brands) => {
        var array=[];
        if(brands && brands.length > 0){
            brands.map(function(a){
                array.push(a.value)
            });
        }
        this.setState({
            selectedBrands: this.multiselectValueToArray(brands),
            queryBrands: array
        });
    };


    handleStatusSelection = (event, index, value) => {
        this.setState({
            selectedStatus: value,
            queryStatus: value
        });
    };

    handleChangeDate = (event, index, date) => {
        this.setState({ date_filter: date } );
        var iniDate = new Date();
        var endDate = new Date();
        if (date === "total") {
            this.setState({ period: false, queryDateIni: null, queryDateIni : null } );
        }
        else if (date === "period"){
            this.setState({
                period: true,
                queryDateIni: null,
                queryDateIni : null }
            );
        }
        else {
            if (date === "today"){
                iniDate.setDate(iniDate.getDate());
                endDate.setDate(endDate.getDate()+1);
            }
            if (date === "yesterday"){
                iniDate.setDate(iniDate.getDate()-1);
                endDate.setDate(endDate.getDate());
            }
            if (date === "last_week"){
                iniDate.setDate(iniDate.getDate()-7);
                endDate.setDate(endDate.getDate()+1);
            }
            if (date === "last_month"){
                iniDate.setDate(iniDate.getDate()-30);
                endDate.setDate(endDate.getDate()+1);
            }
            var dateIniFormatted = moment(iniDate).format('YYYY-MM-DD');
            var dateEndFormatted = moment(endDate).format('YYYY-MM-DD');
            this.setState({
                period: false,
                queryDateIni: dateIniFormatted,
                queryDateEnd: dateEndFormatted
            });
        }
    };

    updateOrder(){
        var order = this.state.order;
        var list = [];

        var cashValue = (this.state.paymentmethod && this.state.paymentmethod.toLowerCase() === 'cash') ? order.totalAmount : 0
        var tpvValue = (this.state.paymentmethod && this.state.paymentmethod.toLowerCase() === 'tpv') ? order.totalAmount : 0
        var appValue = (this.state.paymentmethod && this.state.paymentmethod.toLowerCase() === 'app') ? order.totalAmount : 0
        list.push({typePayment: "Cash",valuePayment: cashValue});
        list.push({typePayment: "TPV",valuePayment: tpvValue});
        list.push({typePayment: "App",valuePayment: appValue});

        var statusChange = {
            status:this.state.status,
            comments:this.state.comments,
            paymentMethod:list
        }
        order.orderStatusChange.push(statusChange);
        db('put','/api/orders/'+this.state.order._id,this.props.token,order)
            .then((response) => {
                console.log("respuesta al actualizar la order")
                console.log(response)
                if(response) {
                    this.setState({
                        open: false,
                        snackMessage: `Pedido modificado correctamente.`,
                        openSnackbar: true
                    });
                }
                else {
                    this.setState({
                        open:false,
                        snackMessage:`Ha ocurrido un error al modificar el pedido.`,
                        openSnackbar:true,
                        loading:false
                    });
                }
                this.getOrders();
            })
            .catch(function(e) {
                console.log("error");
                console.log(e);
            });
    }


    deleteOrder(){
        db('delete','/api/orders/'+this.state.order._id,this.props.token)
            .then((response) => {
                if(response) {
                    this.setState({
                        open: false,
                        snackMessage: `Pedido eliminado correctamente.`,
                        openSnackbar: true
                    });
                }
                else {
                    this.setState({
                        open:false,
                        snackMessage:`Ha ocurrido un error al eliminar el pedido.`,
                        openSnackbar:true,
                        loading: false
                    });
                }
                this.getOrders();
            })
            .catch(function(e) {
                console.log("error");
            });
    }

    getStatusText(status){
        var statusText = ""
        switch (status) {
            case "5":
                statusText = "Recibido";
                break;
            case "10":
                statusText = "En preparación";
                break;
            case "15":
                statusText = "En envío";
                break;
            case "20":
                statusText = "Entregado";
                break;
        }
        return statusText;
    }

    render(){
        
        var { salons_multiselected, brands_multiselected ,selectedShops, selectedBrands, selectedStatus,  date_filter , addStatus} = this.state;

        var addStatusIcon =
            <ContentAdd />
        ;
        if(this.state.addStatus){
            var addStatusIcon =
                <ContentRemove />
            ;
        }



        var loadingContent;

        var customerName = this.state.order ? this.state.order.customerName : "";
        var shopName = this.state.order ? this.state.order.shopName : "";
        var status = this.state.order ? this.state.order.orderStatusChange : "";

        if (this.state.loading) {
            loadingContent = <div style={{marginTop:20, marginBottom:20, marginLeft:5}}> <LinearProgress mode="indeterminate" /> </div>;
        }

        var paymentMethod
        if(this.state.status && this.state.status === 20)
            paymentMethod = (<div><SelectField
                floatingLabelText='Método de pago'
                value={this.state.paymentmethod}
                onChange={(event, index, value)=>this.setState({paymentmethod:value})}
            >
                <MenuItem value="CASH" primaryText="Cash" />
                <MenuItem value="TPV" primaryText="Tarjeta" />
                <MenuItem value="APP" primaryText="App" />
            </SelectField><br/></div>)
        else paymentMethod = ""

        var statusList;

        if(status){
            statusList = status.map((value,a) =>
               <ListItem
                   key={a}
                   leftAvatar={<Avatar icon={<EditorInsertChart />} backgroundColor={blue500} />}
                   primaryText={this.getStatusText(value.status)}
                   secondaryText={moment(value.created).format('YYYY-MM-DD hh:mm:ss')}
               />
           );
        }

        const actions = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Modificar"
                primary={true}
                onClick={() => {this.updateOrder(); this.handleClose();}}
            />,
            <FlatButton
                label="Eliminar"
                primary={true}
                onClick={() => {this.deleteOrder(); this.handleClose();}}
            />
        ];

        var period;

        if(this.state.period){
            period = <div style={{ marginTop: 5}}>
                <div style={{ display: 'inline-flex', height: 10}}>
                    <DatePicker
                        hintText="Fecha de inicio"
                        name="iniDate"
                        mode="landscape"
                        value={this.state.dateIni}
                        onChange={(e, value)=>this.setState({dateIni: value, queryDateIni: moment(value).format('YYYY-MM-DD') })}
                        style={styles.datepickers}
                        textFieldStyle={styles.datepickersTF}
                        inputStyle={{textAlign:'center'}}
                    />
                    <DatePicker
                        hintText="Fecha de fin"
                        name="endDate"
                        mode="landscape"
                        defaultDate={this.state.dateEnd}
                        onChange={(e, value)=>this.setState({dateEnd: value, queryDateEnd: moment(value).format('YYYY-MM-DD') })}
                        style={{display:'inline-block', marginTop:5, marginLeft:10}}
                        textFieldStyle={styles.datepickersTF}
                        inputStyle={{textAlign:'center'}}
                    /><br/>
                </div>
            </div>;
        }

        return(
            <div className="full-width-container">
                <Snackbar
                    open={this.state.openSnackbar}
                    message={this.state.snackMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <h2 style={{padding:20,paddingBottom:0}}>PEDIDOS</h2>
                <div style={{marginTop: 20}}>
                    <div style={{ display: 'inline-flex', height: 10}}>
                        <SelectField
                            floatingLabelText="Fecha"
                            value={date_filter}
                            onChange={this.handleChangeDate}
                            style={{ width: 200, marginTop: -25, padding: 5, marginLeft: 5}}
                        >
                            <MenuItem value='total' primaryText="Total" />
                            <MenuItem value='today' primaryText="Hoy" />
                            <MenuItem value='yesterday' primaryText="Ayer" />
                            <MenuItem value='last_week' primaryText="Última semana" />
                            <MenuItem value='last_month' primaryText="Último mes" />
                            <MenuItem value='period' primaryText="Período" />
                        </SelectField><br/>
                        <SuperSelectField
                            name='selected_salons'
                            multiple
                            floatingLabel='Salones'
                            hintText='Salones...'
                            unCheckedIcon={null}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText)}
                            value={this.arrayToMultiselectValue(selectedShops)}
                            onChange={this.handleShopsMultiSelection}
                            checkPosition='right'
                            style={{ width: 200, marginTop: 7, padding: 5, marginLeft: 5 }}
                        >
                            {salons_multiselected}
                        </SuperSelectField><br/>
                        <SuperSelectField
                            name='selected_brands'
                            multiple
                            floatingLabel='Marcas'
                            hintText='Marcas...'
                            unCheckedIcon={null}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText)}
                            value={this.arrayToMultiselectValue(selectedBrands)}
                            onChange={this.handleBrandsMultiSelection}
                            checkPosition='right'
                            style={{ width: 150, marginTop: 7, padding: 5, marginLeft: 5 }}
                        >
                            {brands_multiselected}
                        </SuperSelectField><br/>
                        <SelectField
                            floatingLabelText="Estados"
                            defaultValue={"todos"}
                            value={selectedStatus}
                            onChange={this.handleStatusSelection}
                            style={{ width: 200, marginTop: -25, padding: 5, marginLeft: 5}}
                        >
                            <MenuItem value={5} primaryText="Recibido" />
                            <MenuItem value={10} primaryText="En preparación" />
                            <MenuItem value={15} primaryText="En envío" />
                            <MenuItem value={20} primaryText="Entregado" />
                        </SelectField><br/>

                        <RaisedButton
                            label="Buscar"
                            primary={true}
                            onClick={this.getOrders}
                            icon={<ActionSearch />}
                            style={styles.button}
                        /><br/>
                    </div>
                    {period}
                </div>
                <br/>
                {loadingContent}
                <DataTables
                    height={'auto'}
                    selectable={false}
                    showRowHover={true}
                    columns={ORDERS_TABLE_COLUMNS}
                    data={this.state.orders}
                    showCheckboxes={false}
                    rowSizeLabel="Filas por página"
                    onCellClick={this.handleCellClick.bind(this)}

                    count={this.state.rowCount}
                    page={this.state.page}
                    onRowSizeChange={this.onRowSizeChange}
                    onNextPageClick={this.handleNextPageClick}
                    onPreviousPageClick={this.handlePreviousPageClick}
                    rowSize={this.state.rowSize}
                    rowSizeList={this.state.rowSizeList}
                />
                <Dialog
                    title="Modificar Pedido"
                    actions={actions}
                    modal={false}
                    open={this.state.openModify}
                    onRequestClose={this.handleClose}
                    titleClassName="dialog-title"
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                >
                    <div style={{ float:'left'}}>
                        <TextField
                            floatingLabelText="Cliente"
                            defaultValue={customerName}
                            disabled={true}
                        />
                    </div>
                    <div style={{ float:'right',marginRight:30, marginBottom:15, width:250}}>
                        <TextField
                            fullWidth={true}
                            floatingLabelText="Tienda"
                            defaultValue={shopName}
                            disabled={true}
                        /><br/>
                        <TextField
                            floatingLabelText="Precio del pedido"
                            defaultValue={this.state.order.totalAmount}
                            disabled={true}
                        />
                    </div>
                    <hr style={{clear:'both'}}/>
                    <div style={{ float:'left'}}>
                        <List>
                            <Subheader inset={true} style={{textAlign:'center', paddingLeft:0}}>Estados</Subheader>
                            {statusList}
                        </List>
                        <FloatingActionButton
                            mini={true}
                            style={{marginLeft:'42%'}}
                            secondary={addStatus}
                            onTouchTap={() => this.setState({ addStatus: !this.state.addStatus })}
                        >
                            {addStatusIcon}
                        </FloatingActionButton>
                    </div>
                    <div id="addStatus" className={"collapse" + (this.state.addStatus ? ' in' : '')} style={{ float:'right',marginRight:30, width:250}}>
                        <div>
                            <SelectField
                                floatingLabelText='Estado del pedido'
                                value={this.state.status}
                                onChange={(event, index, value)=>this.setState({status:value})}
                            >
                                <MenuItem value={5} primaryText="Recibido" />
                                <MenuItem value={10} primaryText="En preparación" />
                                <MenuItem value={15} primaryText="En envío" />
                                <MenuItem value={20} primaryText="Entregado" />
                            </SelectField><br/>
                            <TextField
                                fullWidth={true}
                                floatingLabelText="Comentarios"
                                name="comments"
                                hintText="Comentarios"
                                value={this.state.comments}
                                onChange={(e, value)=>this.setState({ comments: value })}
                            /><br/>
                            {paymentMethod}
                        </div>
                    </div>
                </Dialog>
            </div>
        )
    }

    getBrands(){
        db('get','/api/brands/',this.props.token)
            .then((responseData) => {
                this.setState({
                    brands: responseData
                });
                var nodes = responseData.map(({name, _id}) =>
                    <div key={_id} value={_id} label={name}>{name}</div>
                );
                this.setState({brands_multiselected:nodes})
            })
            .catch(function(varerr) {
                console.log("error del get Brands");
                console.log(varerr);
            });
    }

    getSalons() {
        db('get', '/api/shops', this.props.token).then((responseData) => {
                console.log('log del responsedata en getsalons');
                console.log(responseData);
                responseData.map(function (v) {
                    v.name = v.settings.name;
                });
                var nodes = responseData.map(({name, _id}) =>
                    <div key={_id} value={_id} label={name}>{name}</div>
                );

                this.setState({
                    salons: responseData,
                    salons_multiselected:nodes
                })
            })
            .catch(function (varerr) {
                console.log("error");
                console.log(varerr);
            });
    }

    componentDidMount(){
        this.getOrders();
        this.getSalons();
        this.getBrands();
    }

}

export default SAOrdersPage;