import {DOMAIN} from '../config'

export function db(method,endpoint,token,body){
    let parsedBody = body ? {body:JSON.stringify(body)} : null;
    return fetch(
        DOMAIN+endpoint, {
            method: method,
            dataType: 'json',
            ...parsedBody,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer '+token
            }
        })
        .then(response => response.json())
        .catch(function(e) {
            console.log(e);
        });

}