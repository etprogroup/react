import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, Glyphicon } from 'react-bootstrap'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ImageEdit from 'material-ui/svg-icons/image/edit';
import RaisedButton from 'material-ui/RaisedButton';
import ActionPowerSettingsNew from 'material-ui/svg-icons/action/power-settings-new';
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'

class SATopBar extends Component {

    constructor(props){
        super();
        this.state={
            ddOpacity:0
        }
    }

    toggleProfile(){
            this.setState({ddOpacity:this.state.ddOpacity==1 ? 0 : 1})
    }

    render(){

        var onLogOut = this.props.onLogOut;
        const pointer={0:'none',100:'all'};

        const styles = {
            dropdown:{
                backgroundColor:'#333',
                marginTop:16,
                width:180,
                boxShadow:'rgba(0, 0, 0, 0.5)0px 5px 24px 3px',
                color:'white',
                textAlign:'center',
                opacity:this.state.ddOpacity,
                pointerEvents:pointer[this.state.ddOpacity],
                zIndex: 1
            },
            profilePhoto:{
                width: 100,
                margin:'auto',
                marginTop:15,
                display:'block',
                height: 100,
                backgroundPosition: 'center center',
                backgroundSize: 'cover'
            },
            h1:{
                marginBottom:0
            },
            surname:{
                fontFamily:'HelveticaTh',
                fontSize:24,
            },
            rol:{
                fontFamily:'HelveticaBdC',
                fontSize:20
            },
            detail:{
                fontFamily:'HelveticaTh',
                fontSize:12,
                margin:0
            },
            edit:{
                margin:15
            },
            container:{
                margin: 0,
                borderRadius: 0,
                //position:'fixed',
                //width:'100%',
                zIndex:1301
            },
            button:{
                position:'absolute',
                bottom:0,
                marginBottom:-12
            }
        };

        var userName,userSurname,userEmail,userPhoneNumber,userRoles,imageSrc,id;

        if(this.props.user){
            const user=this.props.user;
            id = "/editar-empleado/"+user._id;
            userEmail = user.email;
            userPhoneNumber = user.phoneNumber;
            userRoles = user.roles;
            userName = 'Super Admin';
            userSurname = user.surname;
            imageSrc = user.profilePhoto || "https://storage.googleapis.com/magnifique-dev/default-profile-pic.jpg";
        } else {
            id = '/';
            userEmail = 'Developing';
            userPhoneNumber = 'Developing';
            userRoles = 'Developing';
            userName = 'Developing';
            userSurname = 'Developing';
            imageSrc =  "https://storage.googleapis.com/magnifique-dev/default-profile-pic.jpg";
        }
        var labelExit = <Translate>SALIR</Translate>

        return (
                <Navbar inverse collapseOnSelect style={styles.container}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to="/appointments"><img className="logo-top" src="/img/logo-top.png" alt="magnifique logo"/></Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight style={{width: 500}}>
                            <NavItem eventKey={0}>
                                <Glyphicon glyph="earphone" style={{marginRight: 10}}/><Translate>Soporte:</Translate> 902.100.100
                            </NavItem>
                            <NavItem
                                eventKey={1}
                            >
                                <Translate>Usuario</Translate>:
                                <span style={{color: '#00B1C6'}} onClick={this.toggleProfile.bind(this)}> {userName} {userSurname}</span>
                                <Glyphicon glyph="user" style={{marginLeft: 10}}/>
                                <div
                                    style={styles.dropdown}
                                    className="profile-dropdown"
                                >
                                    <img className="img-circle" src={imageSrc} alt="" style={styles.profilePhoto}/>
                                    <h1 style={styles.h1}>{userName.toUpperCase()}</h1>
                                    <hr style={{margin:15}}/>
                                    <p style={styles.rol}>{userRoles}</p>
                                    <p style={styles.detail}>{userEmail}</p>
                                    <p style={styles.detail}>{userPhoneNumber}</p>
                                    <Link to={id}>
                                        <FloatingActionButton
                                            mini={true}
                                            secondary={true}
                                            style={styles.edit}
                                            onClick={()=>this.setState({ddOpacity:0})}
                                        >
                                            <ImageEdit />
                                        </FloatingActionButton>
                                    </Link>
                                </div>
                            </NavItem>
                            <NavItem eventKey={2}>
                                <Link to="/login" >
                                    <RaisedButton
                                        className="logout-button"
                                        onClick={onLogOut}
                                        labelPosition="before"
                                        label={labelExit}
                                        icon={<ActionPowerSettingsNew />}
                                        secondary={true}
                                        style={styles.button}
                                    />
                                </Link>
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            )
    }

    componentDidMount() {
        console.log("componentDidMount SATopBar")
        console.log(this.props.user.language)
        reactTranslateChangeLanguage(this.props.user.language)
    }
}

export default SATopBar