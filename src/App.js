import React, { Component } from 'react';
import { BrowserRouter as Router,
    Route,
    Redirect,
    Switch
} from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {DOMAIN} from './config'
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {db} from './components/db';
import Snackbar from 'material-ui/Snackbar';
import ReactStars from 'react-stars';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';

import './App.css';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

import TopBar from './components/TopBar'
import SideMagMenu from './components/SideMagMenu'

import CustomerLogin from './components/pages/Login/CustomerLogin'
import AppointmentPage from './components/pages/Appointments/AppointmentPage'
import UsersPage from './components/pages/Users/UsersPage'
import CustomersPage from './components/pages/Customers/CustomersPage'
import ServicesPage from './components/pages/Services/ServicesPage'
import ProductsPage from './components/pages/Products/ProductsPage'
import ProfilesPage from './components/pages/Profiles/ProfilesPage'
import HomePage from './components/pages/Home/HomePage'
import PostsPage from './components/pages/Posts/PostsPage'
import TillPage from "./components/pages/Till/TillPage";
import StatisticsPage from "./components/pages/Statistics/StatisticsPage";
import process3dSecure from './components/pages/process3dSecure/process3dSecure'
//import PasswordRecoveryPage from "./components/pages/PasswordRecovery/PasswordRecoveryPage";
//import Login from './components/pages/Login/Login'


import SATopBar from './components/superAdmin/SATopBar'
import SASideMenu from './components/superAdmin/SASideMenu'

import SASalonsPage from "./components/superAdmin/Salons/SASalonsPage";
import SACampaignsPage from "./components/superAdmin/Campaigns/SACampaignsPage";
import SAServicesPage from './components/superAdmin/Services/SAServicesPage'
import SABrandsPage from './components/superAdmin/Brands/SABrandsPage'
import SAProductsPage from "./components/superAdmin/Products/SAProductsPage";
import SAOrdersPage from "./components/superAdmin/Orders/SAOrdersPage";

const style={
    container:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height:'100vh'
    },
    box:{
        maxWidth:400,
        backgroundColor:'#343b45',
        color:'white',
        padding:20
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'HelveticaBdC',
        fontSize:36
    },
    inputDate:{
        color:'white',
        fontFamily:'HelveticaTh',
        fontWeight:100,
        textAlign:'center'
    },
    input:{
        color:'white',
        fontFamily:'HelveticaTh',
        fontWeight:100
    },
    inputActive:{
        color:'rgb(0,188,212)'
    },
    underlineDisabledStyle:{
        borderColor:'white'
    },
    button:{
        width:'100%'
    },
    forgotPwd:{
        marginTop:25,
        fontSize:13,
        fontWeight:'bolder',
        cursor:'pointer',
        display:'inline-block'
    },
    input1:{
        color:'white',
        fontFamily:'HelveticaTh',
        fontWeight:100,
        width: 256
    },
    logo:{
        display: 'block',
        margin: '10px auto 0',
        width:150
    },
    timepickers:{
        marginLeft:10
    },
    timepickersTF:{
        width:60
    }
};

const fakeAuth = {
    isAuthenticated: false,
    isSuperAdmin:false,
    authenticate(cb,roles) {
        if (roles==='SuperAdmin')this.isSuperAdmin=true
        else this.isAuthenticated = true
        setTimeout(cb, 100) // fake async
    },
    signout(cb) {
        this.isAuthenticated = false
        this.isSuperAdmin = false
        setTimeout(cb, 100)
    }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
    fakeAuth.isAuthenticated ? (
      <Component {...props} {...rest}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
);

const SAdminRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
    fakeAuth.isSuperAdmin ? (
      <Component {...props} {...rest}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: true,
            openDialog:false,
            mail:null,
            handleChangeList: false
        };
    }

    handleToggle = () => this.setState({open: !this.state.open});

    handleChangeRequestNavDrawer = (open) => {
        
    };

    handleChangeList = (event, value) => {
        this.setState({
            handleChangeList: true
        })
    };

    onLogOut(){
        this.setState({
            handleChangeList: false
        })
        fakeAuth.signout();
        return (<Redirect to="/login" push />)
    }

    onLogin(token,user){
        this.setState({token:token,userData:user});
    }

    state = {
        token: null,
        navDrawerOpen: false,
    };


    render(){
        var sideMenuState = !this.state.open ? {marginLeft:0} : {marginLeft:150};

        let { navDrawerOpen, handleChangeList } = this.state;

        const commonProps = {
            handleChangeList: handleChangeList,
            token: this.state.token,
            salon:  !this.state.userData ? null : this.state.userData.shop ? this.state.userData.shop._id : null,
            user: this.state.userData
        };

        return(
            <MuiThemeProvider>
                <Router>
                    <Switch>
                        <Route
                            exact path="/"
                            render={props => (<HomePage {...props}/>)}
                        />
                        <Route
                            exact path="/login"
                            render={props => (<Login {...props} onLogin={this.onLogin.bind(this)}/>)}
                            />
                        <Route
                            exact path="/process3dSecure"
                            render={props => (<process3dSecure {...props}/>)}
                        />
                        <Route
                            exact path="/recover/:employee"
                            render={props => (<PasswordRecoveryPage {...props} onLogin={this.onLogin.bind(this)}/>)}
                        />
                        <Route
                            exact path="/rating/:appointmentId"
                            render={props => (<RatingPage {...props}/>)}
                        />
                        <Route
                            exact path="/login-cliente/:shop"
                            render={props => (<CustomerLogin {...props} onLogin={this.onLogin.bind(this)}/>)}
                        />
                        <Route
                            exact path="/perfiles"
                            render={() => (<PrivateRoute
                                            component={ProfilesPage}
                                            {...commonProps}
                                        />)}
                        />
                        <Route path="/sadmin/" render={() => (
                                <div>
                                    <SATopBar
                                        user={this.state.userData}
                                        onLogOut={this.onLogOut.bind(this)}
                                    />
                                    <SASideMenu
                                        location={location}
                                        docked={true}
                                        onRequestChangeNavDrawer={this.handleChangeRequestNavDrawer.bind(this)}
                                        onChangeList={this.handleChangeList}
                                        handleToggle={this.handleToggle}
                                        openMenu={this.state.open}
                                        user={this.state.userData}
                                        onLogOut={this.onLogOut.bind(this)}

                                    />
                                    <div className="main-wrapper" style={sideMenuState}>
                                        <SAdminRoute
                                            path="/sadmin/salones"
                                            component={SASalonsPage}
                                            {...commonProps}
                                        />
                                        <SAdminRoute
                                            path="/sadmin/productos"
                                            component={SAProductsPage}
                                            {...commonProps}
                                        />
                                        <SAdminRoute
                                            path="/sadmin/servicios"
                                            component={SAServicesPage}
                                            {...commonProps}
                                        />
                                        <SAdminRoute
                                            path="/sadmin/marcas"
                                            component={SABrandsPage}
                                            {...commonProps}
                                        />
                                        <SAdminRoute
                                            path="/sadmin/campañas"
                                            component={SACampaignsPage}
                                            {...commonProps}
                                        />
                                        <SAdminRoute
                                            path="/sadmin/pedidos"
                                            component={SAOrdersPage}
                                            {...commonProps}
                                        />
                                    </div>
                                </div>
                        )}/>
                        <Route path="/" render={() => (
                                <div>
                                    <TopBar user={this.state.userData}/>
                                    <SideMagMenu
                                        location={location}
                                        docked={true}
                                        handleChangeList={this.state.handleChangeList}
                                        onRequestChangeNavDrawer={this.handleChangeRequestNavDrawer.bind(this)}
                                        onChangeList={this.handleChangeList}
                                        handleToggle={this.handleToggle}
                                        openMenu={this.state.open}
                                        user={this.state.userData}
                                        onLogOut={this.onLogOut.bind(this)}
                                        {...commonProps}
                                    />
                                    <div className="main-wrapper" style={sideMenuState} >
                                        <PrivateRoute
                                            exact path="/appointments"
                                            component={AppointmentPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                                path="/empleados/"
                                                component={UsersPage}
                                                {...commonProps}
                                        />
                                        <PrivateRoute
                                                path="/editar-empleado/:id"
                                                component={UsersPage}
                                                {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/clientes"
                                            component={CustomersPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/servicios"
                                            component={ServicesPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/productos"
                                            component={ProductsPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/posts/:id"
                                            component={PostsPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/caja"
                                            component={TillPage}
                                            {...commonProps}
                                        />
                                        <PrivateRoute
                                            path="/estadisticas"
                                            component={StatisticsPage}
                                            {...commonProps}
                                        />
                                    </div>
                                </div>
                            )}/>
                    </Switch>
                </Router>
            </MuiThemeProvider>
        )
    }

    componentDidMount() {
        if(this.props.user)
            reactTranslateChangeLanguage(this.props.user.language)
    }
    componentDidUpdate() {
        reactTranslateChangeLanguage(this.state.userData.language)
    }
}

//const AuthButton = withRouter(({ history }) => (
//    fakeAuth.isAuthenticated ? (
//        <p>
//            Welcome! <button onClick={() => {
//        fakeAuth.signout(() => history.push('/'))
//      }}>Sign out</button>
//        </p>
//    ) : (
//        <p>You are not logged in.</p>
//    )
//))

class PasswordRecoveryPage extends Component {

    constructor(props) {
        super(props);
        this.state={
            redirectToReferrer: false,
            token:null,
            email:null,
            password:null,
            password2:null,
            openSnackbar: false
        };
        this.updateUser= this.updateUser.bind(this)
    }

    handleRequestCloseSnackbar = () => {
        this.setState({
            openSnackbar: false
        });
    };

    getUserByEmail = (email,token) => {
        var onLogin = this.props.onLogin;
        fetch(
            DOMAIN+'/api/employees/email/'+email, {
                method: 'get',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer '+token
                }
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                reactTranslateChangeLanguage(responseData.language)
                fakeAuth.authenticate(() => {
                    this.setState({ redirectToReferrer: true },onLogin(token,responseData))
                },responseData.roles)
            })
    };


    updateUser(){
        var id = this.props.match.params.employee;
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        //verificar password
        if(this.state.password === this.state.password2){
            var body={
                id:id,
                domain: DOMAIN
            }
            db('post','/api/login/fecha_recuperacion',null,body)
                .then((responseData) => {
                    if(responseData.status == "OK"){
                        var employee = responseData.employee;
                        //verificar mail mismo employee
                        if(employee.email === this.state.email){
                            var changePassword = new Date(employee.changePassword);
                            var now = new Date()
                            var diff = now.getTime() - changePassword.getTime()
                            var mathDiff = Math.floor(diff / (60000))
                            if(mathDiff <= 5){
                                var body={
                                    email:this.state.email,
                                    newPassword: this.state.password
                                }
                                db('post','/api/login/cambiar_contrasena',null,body)
                                    .then((responseData) => {
                                        if(responseData.status == "OK"){

                                            this.setState({
                                                snackMessage: <p><Translate>Contraseña cambiada correctamente.</Translate></p>,
                                                openSnackbar: true
                                            },reactTranslateChangeLanguage(userLang));
                                            this.login()
                                        }
                                    })
                                    .then(() => reactTranslateChangeLanguage(userLang))
                                    .catch(function(varerr) {
                                        console.log(varerr);
                                    });
                            }
                            else{
                                this.setState({
                                    snackMessage: <p><Translate>La duración del enlace ha expirado.</Translate></p>,
                                    openSnackbar: true
                                });
                            }
                        }
                        else{
                            this.setState({
                                snackMessage: <p><Translate>El correo no coincide con el usuario.</Translate></p>,
                                openSnackbar: true
                            });
                        }
                    }
                    else{
                        this.setState({
                            snackMessage: <p><Translate>Ha ocurrido un error al recuperar el usuario.</Translate></p>,
                            openSnackbar: true
                        });
                    }
                })
                .then(() => reactTranslateChangeLanguage(userLang))
                .catch(function(varerr) {
                    console.log(varerr);
                });
        }
        else{
            this.setState({
                snackMessage: <p><Translate>Las contraseñas no coinciden.</Translate></p>,
                openSnackbar: true
            }, reactTranslateChangeLanguage(userLang));
        }
    }

    login = () => {
        var data={
            "grant_type": "password",
            "client_id": "web-ui",
            "client_secret": "A$3cr3tTh4tM4tt3r$T0U$",
            "username": this.state.email,
            "password": this.state.password
        };

        fetch(
            DOMAIN+'/api/oauth/token', {
                method: 'post',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                if (responseData.error==='invalid_grant'){
                    this.setState({userError:'Credenciales no válidas'})
                    return false;
                }else{
                    this.setState({userError:null})
                }
                if(responseData.access_token){
                    this.getUserByEmail(this.state.email,responseData.access_token)
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    };

    redirect(){
        window.location.replace("./../login")
    }


    render() {
        var snackMessage = this.state.snackMessage ? this.state.snackMessage : "";
        var labelEmail = <Translate>Email</Translate>
        var labelPwd = <Translate>Contraseña</Translate>
        var labelPwd2 = <Translate>Repite contraseña</Translate>
        var labelEnviar = <Translate>Enviar</Translate>
        var labelLogIn = <Translate>Volver al LogIn</Translate>
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) {console.log('redirected')
            return (
                <Redirect to={{pathname:'/appointments'}}/>
            )
        }else if(fakeAuth.isSuperAdmin){
            return (
                <Redirect to={{pathname:'/sadmin/salones'}}/>
            )
        }

        return (
            <div style={style.container}>
                <Snackbar
                    open={this.state.openSnackbar}
                    message={snackMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <div style={style.box}>
                    <img alt='magnifique logo' style={style.logo} src="/img/splash.png"/>
                    <TextField
                        floatingLabelText={labelEmail}
                        fullWidth={true}
                        name="email"
                        value={this.state.email}
                        inputStyle={style.input1}
                        floatingLabelStyle={style.input1}
                        floatingLabelShrinkStyle={style.inputActive}
                        onChange={(e, value)=>this.setState({  email: value })}
                    />
                    <TextField
                        floatingLabelText={labelPwd}
                        fullWidth={true}
                        name="pwd1"
                        type='password'
                        value={this.state.password}
                        inputStyle={style.input1}
                        floatingLabelStyle={style.input1}
                        floatingLabelShrinkStyle={style.inputActive}
                        onChange={(e, value)=>this.setState({  password: value })}
                    />
                    <TextField
                        floatingLabelText={labelPwd2}
                        fullWidth={true}
                        name="pwd2"
                        type='password'
                        value={this.state.password2}
                        inputStyle={style.input1}
                        floatingLabelStyle={style.input1}
                        floatingLabelShrinkStyle={style.inputActive}
                        onChange={(e, value)=>this.setState({  password2: value })}
                    />
                    <div style={{textAlign:'center'}}>
                        <RaisedButton
                            label={labelEnviar}
                            secondary={true}
                            fullWidth={false}
                            style={{marginTop:10}}
                            onClick={this.updateUser}
                        />
                        <RaisedButton
                            label={labelLogIn}
                            primary={true}
                            fullWidth={false}
                            style={{marginTop:10, marginLeft:10}}
                            onClick={this.redirect}
                        />
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
    }

    componentDidUpdate() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        setTimeout(() => reactTranslateChangeLanguage(userLang), 50)
    }

}

class RatingPage extends Component {

    constructor(props) {
        super(props);
        this.state={
            appointment:{},
            employee:{},
            shop:{},
            open: false,
            initialCheck:false
        };
        this.ratingChanged = this.ratingChanged.bind(this);
        this.sendRating = this.sendRating.bind(this);
        this.redirect = this.redirect.bind(this);
    }

    ratingChanged (newRating) {
        this.setState({ appointment: { ...this.state.appointment, rating: newRating }} );
    }

    getAppointment = ()=>{
        var appointmentId = (window.location.href).split("rating/");
        db('get', '/api/login/rating/' + appointmentId[1], null)
            .then((responseData) => {
            if (responseData){
                if(responseData.services){
                    var arrayServices = [];
                    for (var i = 0; i < responseData.services.length; i++) {
                        arrayServices.push(responseData.services[i].name)
                    }
                    arrayServices.toString();
                }
                responseData.shop.reviewURL = responseData.shop ? responseData.shop.settings.reviewURL : null;
                const appointmentDate = responseData.appointment ? new Date(responseData.appointment.start) : null;
                const appointmentIniHour = responseData.appointment ? new Date(responseData.appointment.start) : null;
                const appointmentEndHour = responseData.appointment ? new Date(responseData.appointment.end) : null;
                this.setState({
                    appointment:responseData.appointment,
                    employee:responseData.employee,
                    shop: responseData.shop,
                    services: arrayServices,
                    appointmentDate: appointmentDate,
                    appointmentIniHour: appointmentIniHour,
                    appointmentEndHour: appointmentEndHour
                });
            }
            })
            .then(() => {
                if (this.state.appointment.rating){
                    this.setState({initialCheck:true, open:true});
                }
                else {
                    this.setState({initialCheck:false, open:false});
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    };

    sendRating(){

        var appointment={
            "rating": this.state.appointment.rating,
            "reviewDetails": this.state.appointment.reviewDetails,
            "domainLink": DOMAIN
        };

        db('put', '/api/login/appointments/' + this.state.appointment._id, null, appointment)
            .then((response) => {
                if(response) {
                    if (this.state.appointment.rating > 3){
                        this.setState({open: true});
                    }
                    else {
                        this.backHome();
                    }

                }
            })
            .catch(function(e) {
                console.log(e);
            });
    }

    backHome(){
        window.location.replace("./../")
    };

    redirect(){
        const reviewURL = this.state.shop ? this.state.shop.reviewURL : '';
        if(this.state.appointment.rating < 3) {
            setTimeout(function () {
                window.location.replace("./../")
            }, 1000);
        }
        else {
            if (this.state.shop.reviewURL){
                setTimeout(function () {
                    window.location.replace(reviewURL)
                }, 1000);
            }
            else {
                setTimeout(function () {
                    window.location.replace("./../")
                }, 1000);
            }
        }
    }

    render() {
        var labelHome = this.state.initialCheck ? "Volver al Home" : "Más tarde";
        const actions = [
            <FlatButton
                label="Ir"
                primary={true}
                onTouchTap={() => this.redirect()}
            />,
            <FlatButton
                label={labelHome}
                secondary={true}
                style={{marginLeft:5}}
                onTouchTap={() => this.backHome()}
            />
        ];

        if(this.state.initialCheck){
            actions.splice(0,1);
        }

        var labelEnviar = <Translate>Enviar</Translate>
        var labelBack = <Translate>Volver al Home</Translate>
        const employeeName = this.state.employee ? this.state.employee.name : null;
        const employeeSurname = this.state.employee ? this.state.employee.surname : null;
        const employeeFullName = employeeName+" "+employeeSurname;
        const rating = this.state.appointment ? this.state.appointment.rating : null;
        const reviewDetails = this.state.appointment ? this.state.appointment.reviewDetails : null;
        return (
            <div style={style.container}>
                <div style={style.box}>
                    <Dialog
                        title="Opinión del salón"
                        titleClassName="dialog-title"
                        actions={actions}
                        modal={true}
                        open={this.state.open}
                        contentStyle={{width:600}}
                        autoScrollBodyContent={true}
                    >
                        <div style={{textAlign:'center'}}>
                            <h2>¡Gracias por tu valoración!</h2>
                            {(!this.state.initialCheck) &&
                                <h2>¿Deseas ir a la página web del salón y dejarles una opinión?</h2>
                            }
                        </div>
                    </Dialog>
                    <img alt='magnifique logo' style={style.logo} src="/img/splash.png"/>
                    <div id="aligningDiv" style={{textAlign:'center'}}>
                    <h3 style={{textDecoration:'underline',fontSize:18}}>Datos de la cita</h3>
                    <div style={{ display: 'inline-flex'}}>
                        <DatePicker
                            name="date"
                            value={this.state.appointmentDate}
                            mode="landscape"
                            textFieldStyle={{ width: 100 }}
                            disabled={true}
                            inputStyle={style.inputDate}
                            underlineDisabledStyle={style.underlineDisabledStyle}
                        />
                        <TimePicker
                            name="iniTimeF"
                            value={this.state.appointmentIniHour}
                            format="24hr"
                            style={style.timepickers}
                            textFieldStyle={style.timepickersTF}
                            disabled={true}
                            inputStyle={style.inputDate}
                            underlineDisabledStyle={style.underlineDisabledStyle}
                        />
                        <p style={{marginTop:12, marginLeft:10}}>—</p>
                        <TimePicker
                            name="endTimeF"
                            value={this.state.appointmentEndHour}
                            format="24hr"
                            style={style.timepickers}
                            textFieldStyle={style.timepickersTF}
                            disabled={true}
                            inputStyle={style.inputDate}
                            underlineDisabledStyle={style.underlineDisabledStyle}
                        /><br/>
                    </div>
                    <TextField
                        floatingLabelText="Estilista"
                        fullWidth={false}
                        name="employee"
                        value={employeeFullName}
                        disabled={true}
                        inputStyle={style.input}
                        floatingLabelStyle={style.input}
                        underlineDisabledStyle={style.underlineDisabledStyle}
                    />
                    </div>
                    <TextField
                        floatingLabelText="Servicios"
                        fullWidth={false}
                        name="reviewDetails"
                        multiLine={true}
                        value={this.state.services}
                        disabled={true}
                        style={{marginLeft:'15%'}}
                        textareaStyle={style.input}
                        floatingLabelStyle={style.input}
                        underlineDisabledStyle={style.underlineDisabledStyle}
                    />
                        <hr/>
                    <h3 style={{textDecoration:'underline',fontSize:18, textAlign:'center'}}>Valoración</h3>
                    <div style={{marginLeft:'30%'}}>
                        <ReactStars
                            count={5}
                            value={rating}
                            onChange={this.ratingChanged}
                            size={35}
                            color2={'#ffd700'}
                        />
                    </div>
                    <TextField
                        floatingLabelText="Comentario"
                        fullWidth={false}
                        multiLine={true}
                        name="reviewDetails"
                        value={reviewDetails}
                        onChange={(e, value)=>this.setState({ appointment: { ...this.state.appointment, reviewDetails: value }} )}
                        style={{marginLeft:'15%'}}
                        textareaStyle={style.input}
                        floatingLabelStyle={style.input}
                        rowsMax={4}
                    />
                    <div id="formButtons" style={{textAlign:'center'}}>
                        <RaisedButton
                            label={labelEnviar}
                            secondary={true}
                            fullWidth={false}
                            style={{marginTop:10}}
                            onClick={this.sendRating}
                        />
                        <RaisedButton
                            label={labelBack}
                            primary={true}
                            fullWidth={false}
                            style={{marginTop:10, marginLeft:10}}
                            onClick={this.backHome}
                        />
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.getAppointment();
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
    }

    componentDidUpdate() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        setTimeout(() => reactTranslateChangeLanguage(userLang), 50)
    }

}

class Login extends Component{

    state = {
        redirectToReferrer: false,
        user: '',
        password: '',
        token:null,
        userError:null,
        passwordError:null,
        openSnackbar: false
    };

    handleRequestCloseSnackbar = () => {
        this.setState({
            openSnackbar: false
        });
    };

    getUserByEmail = (email,token) => {
        var onLogin = this.props.onLogin;
        fetch(
            DOMAIN+'/api/employees/email/'+email, {
                method: 'get',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer '+token
                }
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                fakeAuth.authenticate(() => {
                    this.setState({ redirectToReferrer: true },onLogin(token,responseData))
                },responseData.roles)
            })
    };

    login = () => {
        var data={
            "grant_type": "password",
            "client_id": "web-ui",
            "client_secret": "A$3cr3tTh4tM4tt3r$T0U$",
            "username": this.state.user,
            "password":this.state.password
        };

        fetch(
            DOMAIN+'/api/oauth/token', {
                method: 'post',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                if (responseData.error==='invalid_grant'){
                    this.setState({userError:'Credenciales no válidas'})
                    return false;
                }else{
                    this.setState({userError:null})
                }
                if(responseData.access_token){
                    this.getUserByEmail(this.state.user,responseData.access_token)
                }
            })
            .then(() => {
                var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
                reactTranslateChangeLanguage(userLang)
            })
            .catch(function(e) {
                console.log(e);
            });
    };

    setToken(token){

        this.setState({token:token.access_token});

    }

    enterApp(token){

        var onLogin = this.props.onLogin;
        onLogin(token.access_token)

        fakeAuth.authenticate(() => {
            this.setState({ redirectToReferrer: true })
        })

    }

    closeDialog() {
        this.setState({ openDialog: false });
    }

    recover(){
        this.setState({openDialog:false})
        var body={
            email:this.state.mail,
            domain: DOMAIN
        }
        db('post','/api/login/recuperar',null,body)
            .then((responseData) => {
                if(responseData.status == "OK"){
                    this.setState({
                        snackMessage: <p><Translate>Revisa tu bandeja de entrada.</Translate></p>,
                        openSnackbar: true
                    })
                }
                else{

                }
            })
            .catch(function(varerr) {
                console.log(varerr);
            });

    }

    render() {
        const labelLogin = <Translate>ENTRAR</Translate>;
        const labelPassword = <Translate>Contraseña</Translate>;
        const labelUser = <Translate>Usuario</Translate>;
        const { from } = this.props.location.state  || { from: { pathname: '/' } }
        const path = from.pathname==='/' ? { pathname: '/appointments' } : from;
        const { redirectToReferrer } = this.state;
        var snackMessage = this.state.snackMessage ? this.state.snackMessage : ""

        var actions = [
            <RaisedButton
                label={<Translate>Enviar</Translate>}
                secondary={true}
                disabled={!this.state.mail}
                fullWidth={false}
                style={{marginTop:10}}
                onClick={this.recover.bind(this)}
            />,
            <FlatButton
                label={<Translate>Cancelar</Translate>}
                primary={true}
                onTouchTap={this.closeDialog.bind(this)}
            />
        ];

        if (redirectToReferrer) {console.log('redirected')
            return (
                <Redirect to={path}/>
            )
        }else if(fakeAuth.isSuperAdmin){
            return (
                <Redirect to={{pathname:'/sadmin/salones'}}/>
            )
        }

        return (
            <div style={style.container}>
                <Snackbar
                    open={this.state.openSnackbar}
                    message={snackMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <div style={style.box}>
                    <img className="logo-top" src="/img/splash.png" alt="magnifique logo" style={{display:'block',margin:'20px auto 80px'}}/>
                    <p style={style.h1}><Translate>Autenticación</Translate></p>
                    <TextField
                        floatingLabelText={labelUser}
                        name="name"
                        defaultValue={null}
                        errorText={this.state.userError}
                        inputStyle={style.input}
                        floatingLabelStyle={style.input}
                        floatingLabelShrinkStyle={style.inputActive}
                        onChange={(e, value)=>this.setState({ user:  value })}
                    /><br/>
                    <TextField
                        floatingLabelText={labelPassword}
                        name="password"
                        inputStyle={style.input}
                        errorText={this.state.passwordError}
                        floatingLabelStyle={style.input}
                        floatingLabelShrinkStyle={style.inputActive}
                        type="password"
                        onChange={(e, value)=>this.setState({  password: value })}
                    /><br/>
                    <div style={{textAlign:'center'}}>
                    <p style={style.forgotPwd} onClick={() => {this.setState({openDialog:true, mail:''})} }><Translate>He olvidado la contraseña</Translate></p>
                    </div>
                    <RaisedButton
                        onClick={this.login}
                        label={labelLogin}
                        primary={true}
                        style={style.button}
                    />
                </div>
                <Dialog
                    title={<p><Translate>Recuperar contraseña</Translate></p>}
                    titleClassName="dialog-title"
                    modal={true}
                    actions={actions}
                    open={this.state.openDialog}
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                >
                    <p><Translate>Introduce el correo con el que te registraste. Te enviaremos un correo a la dirección facilitada para resetear tu contraseña.</Translate></p>
                    <div style={{textAlign:'center'}}>
                    <TextField
                        floatingLabelText={<Translate>Correo</Translate>}
                        name="mail"
                        defaultValue={this.state.mail}
                        onChange={(e, value)=>this.setState({ mail: value })}
                    />
                    </div>
                </Dialog>
            </div>
        )
    }

    componentDidMount() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
    }

    componentDidUpdate() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        setTimeout(() => reactTranslateChangeLanguage(userLang), 50)
    }

    componentWillMount(){//For production
    }


}

export default App;
