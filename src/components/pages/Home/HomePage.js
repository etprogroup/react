﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import {postImage} from '../../helpers'
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import { Link } from 'react-router-dom';
import './carousel2.css';
import ScrollableAnchor from 'react-scrollable-anchor'
import ScrollAnimation from './scroll-animation';
import '../../../../node_modules/animate.css/animate.min.css';
var Carousel = require('react-responsive-carousel').Carousel;
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import SuperSelectField from '../../SuperSelectField'
import {db} from '../../db'
import Snackbar from 'material-ui/Snackbar';
import LinearProgress from 'material-ui/LinearProgress';
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'
import Recaptcha from 'react-recaptcha';
import scriptjs from 'scriptjs';

const style={
    loginButton:{
        float: 'right',
        backgroundColor: '#00b1c6',
        color: '#d8d8d8',
        marginRight: '2%',
        marginTop: 15,
        display: 'block'
    },
    registerButton:{
        float: 'right',
        backgroundColor: '#c60000',
        color: '#d8d8d8',
        marginRight: '2%',
        marginTop: 15,
        display: 'block'
    },
    tendenciasButton:{
        backgroundColor: '#00b1c6',
        color: '#d8d8d8',
        height: 50,
        marginBottom: 2,
        display: 'block',
        zIndex: 0
    },
    navButton:{
        float: 'right',
        backgroundColor: 'transparent',
        color: '#d8d8d8',
        marginRight: '2%',
        marginTop: 15,
        display: 'block',
        opacity: 1
    },
    buttons:{
        textAlign: 'center',
        zIndex: -1,
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    downloadButton2:{
        marginTop: 20,
        border: '5 solid',
        borderColor: '#00b1c6',
        marginRight: 20,
        height: 50,
        width: 175,
        zIndex: 0,
        textAlign: 'center'
    },
    downloadButton3:{
        marginTop: 20,
        border: '2 solid',
        borderColor: '#ffffff',
        borderRadius: 100,
        marginRight: 20,
        width: 40,
        height: 40,
        zIndex: 0,
        float: 'right',
        color: 'white'
    },
    imageTestimonial:{
        width: '25%',
        height: '25%',
        paddingBottom: 30
    },
};

class HomePage extends Component {

    constructor(props){
        super(props);
        this.state = {
            open: false,
            salon: {},
            patherror:null,
            emailError:null,
            loading: false,
            selectedProfiles: []
        };
    }

    redirect(x){
        if (x === 'appstore'){
            window.location.href="https://itunes.apple.com/es/app/magnifique/id1252960188?mt=8";
        }
        if (x === 'playstore'){
            window.location.href="https://play.google.com/store/apps/details?id=com.magnifique&hl=es";
        }
        if (x === 'facebook'){
            window.location.href="https://www.facebook.com/urb16/";
        }
        if (x === 'twitter'){
            window.location.href="https://twitter.com/";
        }
        if (x === 'instagram'){
            window.location.href="https://www.instagram.com/urb16/";
        }
        if (x === 'ohlala'){
            window.location.href="https://magnifique.club/category/tendencias/oohlala/";
        }
        if (x === 'ohmondieu'){
            window.location.href="https://magnifique.club/category/tendencias/ohmondieu/";
        }
        if (x === 'blog'){
            window.location.href="https://magnifique.club/";
        }
    }

    getProfiles() {
        console.log("getProfiles..")
        db('get', '/api/login/profiles/', null).then((response) => {
            var nodes = response.map(({name, _id}) =>
                <div key={_id} value={_id} label={name}>{name}</div>
            );
            this.setState({
                profiles: nodes,
                profileList: response
            })
        }).catch(function (err) {
            console.log("error");
            console.log(err)
        });
    }

    // imagen
    openFileDialog = ()=> {
        var fileInputDom = ReactDOM.findDOMNode(this.refs.input)
        fileInputDom.click()
    };
    _handleSubmit = (e, cb)=> {
        e.preventDefault();
        postImage(this.state.file, cb)
    };
    _handleImageChange = (e)=> {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(file)
    };

    saveNewSalon = async(imageUrl)=> {
        this.setState({loading: true})
        var {selectedProfiles, services, profiles} = this.state.salon;
        profiles = profiles ? profiles : this.state.salon.settings ? this.state.salon.settings.profiles : null;

        var validation = true;

        if (!this.state.shopName) {
            this.setState({shopNameError: 'Este campo es obligatorio'});
            validation = false;
        }
        else {
            this.setState({shopNameError: null})
        }
        if (!profiles || profiles === null || profiles.length === 0) {
            this.setState({profilesError:(<div style={{color:'red',fontSize:12,marginTop:5}}>No has seleccionado ningún perfil</div>)})
            validation = false;
        }
        else {
            this.setState({profilesError: null})
        }
        if (!imageUrl || imageUrl === null) {
            this.setState({patherror:(<div style={{color:'red',fontSize:12}}><Translate>No has seleccionado ningún logotipo</Translate></div>)})
            validation = false;
        }
        else {
            this.setState({patherror: null})
        }
        if (validation) {
            if(selectedProfiles){
                services = await db('get', '/api/login/services/', null).then((allServices)=> {
                    let servArr = [];
                    selectedProfiles.forEach((profile)=> {
                        allServices.forEach((service)=> {
                            if (profile.label === service.profile)servArr.push(service)
                        })
                    });
                    return servArr
                });
            }
            var name = this.state.shopName;
            var slug = this.state.shopName.toLowerCase().trim();
            var domain = 'domain.es'
            var userEmail = this.state.email
            var userPassword = this.state.password

            var body = {
                services: services,
                settings: {
                    name: name,
                    adminMail: userEmail,
                    adminPwd: userPassword,
                    nameSlug: slug,
                    domain: domain,
                    profiles: profiles,
                    mainPicture: {
                        title: slug + '-logo',
                        path: imageUrl
                    }
                },
                location: {
                    address: 'address',
                    city: 'city',
                    country: 'ES'
                }
            };

            db('post', '/api/login/shops/', null, body)
                .then((responseData) => {
                    console.log(responseData)
                    if(responseData && responseData.status === 'OK') {
                        this.setState({
                            open: false,
                            loading: false,
                            snackMessage: <Translate>Salón creado correctamente.</Translate>,
                            openSnackbar: true
                        });
                    }
                    else {
                        this.setState({
                            loading: false,
                            emailError: 'Este usuario ya existe'
                        });
                    }
                })
                .catch(function (err) {
                    console.log('error')
                    console.log(err);
                });
        }
    };

    selectionsRenderer = (values, hintText) => {
        if (!values) return hintText
        const {value, label} = values
        if (Array.isArray(values)) {
            return values.length
                ? values.map(({value, label}) => label || value).join(', ')
                : hintText
        }
        else if (label || value) return label || value
        else return hintText
    }

    arrayToMultiselectValue(value) {
        var array = [];
        if (typeof (value) == 'undefined' || value === null) {
            if (this.state.salon && this.state.salon.settings) {
                this.state.salon.settings.profiles.map(function (a) {
                    array.push({"value": a._id, label: a.name})
                });
            } else return [];
        }
        else {
            value.map(function (a) {
                array.push({"value": a.value, label: a.label});
            });
        }
        return array
    }

    multiselectValueToArray(value) {
        var array = [];
        value.map(function (a) {
            array.push(a.value)
        });
        return array
    }

    handleProfilesSelection = (profiles) => {
        if (profiles) {
            this.setState({profilesError: null})
        }
        this.setState({
            salon: {
                ...this.state.salon,
                profiles: profiles.length ? this.multiselectValueToArray(profiles) : profiles,
                selectedProfiles: profiles
            }
        })
    }

    render(){
        var {patherror, imagePreviewUrl} = this.state
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (
                <img
                    alt=""
                    src={imagePreviewUrl}
                    style={
                   {maxWidth:200}
                    }/>);
        }
        var actions = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onTouchTap={() => this.setState({open: false})}
            />,
            <FlatButton
                label="Registrar"
                secondary={true}
                style={{marginLeft:5}}
                onTouchTap={(e) => {this._handleSubmit(e,this.saveNewSalon)}}
            />
        ];

        //Captcha functions
        // site key
        const sitekey = '6Lcy3UkUAAAAADoM-ug62JBK2I3dkSNiv3oUmwZP';

        // specifying your onload callback function
        const callback = () => {
            console.log('Recaptcha loaded correctly.');
        };

        const verifyCallback = (response) => {
            console.log(response);
        };

        const expiredCallback = () => {
            console.log(`Recaptcha expired.`);
        };

        // define a variable to store the recaptcha instance
        let recaptchaInstance;
        
        //End of captcha functions

        var selectedProfiles = this.state.salon ?  this.state.salon.selectedProfiles : [];
        const { profiles } = this.state

        var loadingContent;

        if (this.state.loading) {
            loadingContent = <div style={{marginTop:20, marginBottom:20, marginLeft:5}}> <LinearProgress mode="indeterminate" /> </div>;
        }

        return(
            <div id="container">
                <Snackbar
                    open={this.state.openSnackbar}
                    message={this.state.snackMessage}
                    autoHideDuration={3000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <div className="body">
                    <Dialog
                        title="Registrar nuevo salón"
                        titleClassName="dialog-title"
                        actions={actions}
                        modal={true}
                        open={this.state.open}
                        contentStyle={{width:300}}
                        autoScrollBodyContent={true}
                    >
                        {loadingContent}
                        <p style={{fontSize:12, fontWeight:'bold', marginTop:20}}>Administrador</p>
                        <TextField
                            floatingLabelText="Email"
                            value={this.state.email}
                            errorText={this.state.emailError}
                            onChange={(e, value)=>this.setState({ email: value } )}
                            style={{ width: 250, marginTop: -30 }}
                        /><br/>
                        <TextField
                            floatingLabelText="Clave"
                            value={this.state.password}
                            type="password"
                            onChange={(e, value)=>this.setState({ password: value } )}
                            style={{ width: 250, marginTop: -17 }}
                        /><br/>
                        <hr/>
                        <p style={{fontSize:12, fontWeight:'bold'}}>Salón</p>
                        <TextField
                            floatingLabelText="Nombre del Salón"
                            value={this.state.shopName}
                            onChange={(e, value)=>this.setState({ shopName: value } )}
                            style={{ width: 250, marginTop: -30 }}
                        /><br/><br/>
                        <SuperSelectField
                            name='perfiles'
                            multiple
                            floatingLabel='Perfiles del salón'
                            hintText='Selecciona perfiles...'
                            unCheckedIcon={null}
                            menuCloseButton={(<FlatButton label='Cerrar' hoverColor={'lightSalmon'} />)}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText)}
                            value={this.arrayToMultiselectValue(selectedProfiles)}
                            onChange={this.handleProfilesSelection}
                            checkPosition='right'
                            style={{ width: 250 }}
                        >
                            {profiles}
                        </SuperSelectField>
                        <br/>
                        <div style={{textAlign:'center'}}>
                            <RaisedButton
                                label={<Translate>Escoger logo del salon</Translate>}
                                onClick={this.openFileDialog}
                            />
                            <form onSubmit={this._handleSubmit}>
                                <input type="file" ref='input' style={{ display: 'none' }}
                                       onChange={this._handleImageChange}/>
                            </form>
                            <br/>
                            <div id="patherror" className={"collapse" + (!this.state.imagePreviewUrl ? ' in' : '')}>
                                {patherror}
                            </div>
                            {$imagePreview}
                        </div>
                        <div style={{marginLeft:'15%'}}>
                            <Recaptcha
                                ref={e => recaptchaInstance = e}
                                sitekey={sitekey}
                                size="compact"
                                render="explicit"
                                verifyCallback={verifyCallback}
                                onloadCallback={callback}
                                expiredCallback={expiredCallback}
                            />
                        </div>
                    </Dialog>
                <div id="nav">
                    <Link to="/login">
                        <FlatButton
                            style={style.loginButton}
                            label="Login"
                            primary={false}
                        />
                    </Link>
                        <FlatButton
                            style={style.registerButton}
                            label="REGISTRAR MI SALÓN"
                            primary={false}
                            onTouchTap={() => this.setState({open: true, email: '', password:'', shopName: ''})}
                        />
                        <FlatButton
                            style={style.navButton}
                            label="Descargar"
                            primary={false}
                            href="#section2"
                        />
                        <FlatButton
                            style={style.navButton}
                            label="Blog"
                            primary={false}
                            onClick={()=>this.redirect('blog')}

                        />
                        <FlatButton
                            style={style.navButton}
                            label="Características"
                            primary={false}
                            href="#section1"
                        />
                        <FlatButton
                            style={style.navButton}
                            label="Inicio"
                            primary={false}
                            href="#section0"
                        />
                    <span>
                        <img src="/img/logo.png" alt="magnifique-logo" style={{maxWidth: '100%', maxHeight: '100%', textAlign: 'left', marginLeft: '2%', marginTop: 8, display: 'block'}} />
                    </span>
                </div>
                    <ScrollableAnchor id={'section0'}>
                    <section id="inicio">
                        <div className="containerLeftIni">
                            <ScrollAnimation animateIn="slideInLeft" animateOnce={true}>
                            <div className="wrapper-section0">
                    <span>
                        <img src="/img/splash.png" alt="magnifique-logo-splash" style={{maxWidth: '50%', maxHeight: '50%', marginTop: 150, display: 'block', marginLeft: 'auto', marginRight: 'auto'}} />
                    </span>
                    <h1 id="h1down" style={{color: 'white', fontSize: 32, fontFamily: 'Open Sans, sans-serif', fontWeight: 'bolder'}}>¡Descarga gratis tu aplicación <br/> de belleza favorita!</h1>
                <div className="buttons" style={style.buttons}>
                    <span>
                        <a href="https://itunes.apple.com/es/app/magnifique/id1252960188?mt=8"><img src="/img/app-store.png" alt="magnifique-ios" style={{width: '30%', height: '30%', display: 'inline-block', marginTop: 30}} /></a>
                    </span>
                    <span>
                        <a href="https://play.google.com/store/apps/details?id=com.magnifique&hl=es"><img src="/img/play-store.png" alt="magnifique-play" style={{width: '30%', height: '30%', display: 'inline-block', marginLeft: 20, marginTop: 30}} /></a>
                    </span>
                </div>
                </div>
                            </ScrollAnimation>
                    </div>

                    <div className="containerRightIni">
                    <span>
                        <img id="mobileApp" src="/img/mobile-right.png" alt="magnifique-app"/>
                    </span>
                    </div>
                    </section>
                    </ScrollableAnchor>

                    <ScrollableAnchor id={'section1'}>
                    <section id="firstFeature">
                    <div className="containerLeft" style={{animationName: 'mymoveLeft', animationDuration: '3s'}}>
                        <div className="circle">NEWS HAIR STYLE</div>
                        <span id="iphone-images">
                            <img id="iphone-style1" src="/img/iphone-estilo1.png" alt="style1"/>
                            <img id="iphone-style2" src="/img/iphone-estilo2.png" alt="style1"/>
                        </span>
                    </div>/

                    <div className="containerRight">
                        <ScrollAnimation animateIn="slideInRight" animateOnce={true}>
                        <div className="tendencias">
                        <h1 style={{color:'#aaaaaa', fontSize: 32, fontFamily: 'Open Sans, sans-serif', fontWeight: 'bolder', marginBottom: 20, marginTop: 50}}>Tendencias</h1>
                        <FlatButton
                            style={style.tendenciasButton}
                            label="Estilo y actualidad"
                            primary={false}
                            onClick={()=>this.redirect('ohlala')}
                        />
                            <br/>
                        <FlatButton
                            style={style.tendenciasButton}
                            label="¡Todo lo que te interesa!"
                            primary={false}
                            onClick={()=>this.redirect('ohmondieu')}
                        />
                            <p style={{fontWeight: 'bold', fontSize: 16, color: '#d3d3d3', marginTop: 20, lineHeight: 1.5}}> Todas las noticias y actualidad en estilisto y peluquería profesional, <br/> novedades, información, consejos y todos los looks de creados por los <br/> estilistas más reconocidos.</p>
                            <hr style={{float: 'left', width: '80%', color: '#5b5b5b'}}/>
                            <p style={{fontWeight: 'bold', fontSize: 18, color:'white', paddingTop: 50}}>Colecciones</p>
                            <ul style={{fontWeight: 'bold', fontSize: 16, color: '#d3d3d3', listStyleType: 'none', lineHeight: 2}}>
                                <li>
                                    <p>     ✓ Para ellos</p>
                                </li>
                                <li>
                                    <p>     ✓ Para ellas</p>
                                </li>
                            </ul>
                            <RaisedButton
                                style={style.downloadButton2}
                                label="¡Descárgala ya!"
                                primary={true}
                                href="#section2"
                            />
                        </div>
                        </ScrollAnimation>
                    </div>
                        </section>
                    </ScrollableAnchor>
                    <section id="secondFeature">
                        <ScrollAnimation animateIn="slideInLeft" animateOnce={true}>
                        <div className="containerLeft2" style={{animationName: 'mymoveLeft', animationDuration: '2s'}}>
                        <h2 style={{textAlign: 'right', color: 'white', fontSize: 32, fontFamily: 'Open Sans, sans-serif', fontWeight: 'bolder', marginBottom: 0, marginTop: 120, letterSpacing: 1}}>Reserva una cita</h2>
                            <h3 style={{textAlign: 'right', color: 'white',fontFamily: 'Open Sans, sans-serif', fontSize: 32, fontWeight: 400, marginTop: 0}}>en tu centro de belleza y con tu estilista favorito</h3>
                            <p style={{textAlign:'right', color: 'white', fontFamily: 'Open Sans, sans-serif', fontSize: 16}}>Tu centro de belleza favorito, ahora disponible todos los días del año</p>

                            <FlatButton
                                href="#section2"
                                backgroundColor="#a4c639"
                                hoverColor="#8AA62F"
                                icon={<ActionAndroid />}
                                style={style.downloadButton3}
                            />
                            <FlatButton
                                href="#section2"
                                backgroundColor="#a4c639"
                                hoverColor="#8AA62F"
                                icon={<img src="/img/apple-icon2.png" alt="Apple" width="25" height="25"/>}
                                style={style.downloadButton3}
                            />

                        </div>
                        </ScrollAnimation>
                        <div className="containerRight2">
                        <span id="featureMobile" style={{marginLeft: 80}}>
                            <img id="mobile" src="/img/mobile.png" alt="mobile"/>
                        </span>
                        </div>

                    </section>

                    <section id="thirdFeature">
                        <div className="containerLeft">
                        <span id="promo" style={{marginLeft: 100}}>
                            <img id="promo-1" style={{marginTop: 100}} src="/img/promo-1.png" alt="promo1"/>
                            <img id="promo-2" style={{marginTop: 100}} src="/img/promo-2.png" alt="promo2"/>
                        </span>
                        </div>

                        <div className="containerRight">
                            <ScrollAnimation animateIn="slideInRight" animateOnce={true}>
                            <div className="wrapper-thirdFeature" style={{marginTop: 110}}>
                        <h1 style={{color: '#d3d3d3', fontSize: 32, fontFamily: 'Open Sans, sans-serif', fontWeight: 'bolder', position: 'relative'}}>Promociones y ofertas exclusivas</h1>
                        <h1 style={{backgroundColor: '#00b1c6', textAlign: 'center', lineHeight: '50px', color: 'white', height: 50, width: 350, fontSize: 20, fontFamily: 'Open Sans, sans-serif', marginTop: 20, marginBottom: 20, position: 'relative'}}>¡Todos los servicios que necesitas!</h1>
                          <p style={{color: '#d3d3d3', marginBottom: 20, fontSize: 18, position: 'relative'}}>Con magnifique podrás disfrutar de las mejores servicios y ofertas reservadas en exclusiva para tí</p>
                           <RaisedButton
                                style={{marginTop: 20, border: '5 solid', borderColor: '#00b1c6', marginRight: 20, height: 50, width: 200, zIndex: 0, textAlign: 'center'}}
                                label="Descargar ahora"
                                primary={true}
                                href="#section2"
                            />
                        </div>
                            </ScrollAnimation>
                        </div>

                    </section>
                    <section id="fourthFeature">
                        <div className="containerCenter">
                            <div className="text-combo">
                            <ScrollAnimation animateIn="slideInRight" animateOnce={true}>
                            <div className="textBox-header" style={{position: 'relative'}}>
                                <h2 style={{textAlign: 'left', color: 'white', fontFamily: 'Open Sans, sans-serif', fontSize: 32, fontWeight: 400, paddingTop: 50, marginLeft: 30, letterSpacing: 3, position: 'relative'}}>Premiamos tu fidelidad</h2>
                            </div>
                            </ScrollAnimation>
                             <ScrollAnimation animateIn="slideInLeft" animateOnce={true}>
                            <div className="textBox" style={{position: 'relative'}}>
                                <h4 className="title-properties">Gana dinero</h4>
                                <p className="text-properties">Cada vez que visites tu centro de belleza favorito, ganarás dinero que será asignado
                                    a tu monedero personal. Olvídate de las targetas fidelidad</p>
                                <hr style={{textAlign: 'center', width: '80%'}}/>
                                <h4 className="title-properties">Regala descuentos</h4>
                                <p className="text-properties">Invita a tus amigas y amigos a que vayan a tu centro de belleza habitual.
                                    Tanto ellos como tú obtendreis regalos e importantes descuentos para las próximas visitas.</p>
                                <hr style={{textAlign: 'center', width: '80%'}}/>
                                <h4 className="title-properties">Canjea tus premios</h4>
                                <p className="text-properties">Ser miembro del club Magnifique tiene sus ventajas y por ello podrás llegar a disfrutar de tus recompensas
                                    siempre que quieras y donde quieras.</p>
                            </div>
                             </ScrollAnimation>
                            </div>
                        </div>
                    </section>
                    <section id="fifthFeature">
                        <div className="carousel-container">
                        <Carousel
                            showArrows={false}
                            showThumbs={false}
                            showStatus={false}
                            showIndicators={false}
                            autoPlay={true}
                            infiniteLoop={true}
                            interval={4000}
                        >
                            <div>
                                <img className="imageTestimonial" src="/img/man-1.png" alt="imageTestimonial" style={style.imageTestimonial}/>
                                <p style={{color: 'white'}}>Manolo Martínez <br/> Usar la aplicación me ha ahorrado mucho tiempo.</p>
                            </div>
                            <div>
                                <img className="imageTestimonial" src="/img/man-2.png" alt="imageTestimonial" style={style.imageTestimonial}/>
                                <p style={{color: 'white'}}>Raúl Vasco <br/> Me encanta, es tan fácil reservar como pagar a través de la App. Ahora puedo estar más cerca de mi estilista</p>
                            </div>
                            <div>
                                <img className="imageTestimonial" src="/img/man-3.png" alt="imageTestimonial" style={style.imageTestimonial}/>
                                <p style={{color: 'white'}}>Maria Pineda <br/> Antes me daba mucha pereza ir a la peluquería, pero ahora con la aplicación todo es mucho más sencillo.</p>
                            </div>
                            <div>
                                <img className="imageTestimonial" src="/img/man-4.png" alt="imageTestimonial" style={style.imageTestimonial}/>
                                <p style={{color: 'white'}}>Clara López <br/> Muy intuitiva y fácil de usar.</p>
                            </div>
                        </Carousel>
                        </div>
                    </section>

                    <section id="sixthFeature">
                        <div className="containerLeft3">
                            <span id="iphone-pago-span">
                            <img id="iphone-pago" src="/img/iphone-pago.png" alt="mobile-payment"/>
                        </span>
                        </div>
                        <div className="containerRight3">
                            <div className="payment-text-box">
                                <ScrollAnimation animateIn="slideInRight" animateOnce={true}>
                                <div className="payment-group" style={{position: 'relative'}}>
                                    <img id="payment-image1" src="/img/payment-1.png" alt="payment-1" style={{width: 80, height: 72}}/>
                                    <h4 className="payment-title-properties">Pago seguro "One click"</h4>
                                    <p className="payment-text-properties">Reserva y paga tus compras online de forma 100% segura. Aplicamos los mismos sistemas de seguridad que tienen los bancos</p>
                                </div>
                                </ScrollAnimation>

                                <ScrollAnimation animateIn="slideInRight" animateOnce= {true}>
                                <div className="payment-group" style={{position: 'relative'}}>
                                    <img id="payment-image1" src="/img/payment-2.png" alt="payment-2" style={{width: 80, height: 72}}/>
                                    <h4 className="payment-title-properties">Consulta tus compras</h4>
                                    <p className="payment-text-properties">Lista y consulta todas las compras que hayas realizado a través de la App con total transparencia</p>
                                </div>
                                </ScrollAnimation>

                                <ScrollAnimation animateIn="slideInRight" animateOnce={true}>
                                <div className="payment-group" style={{position: 'relative'}}>
                                    <img id="payment-image1" src="/img/payment-3.png" alt="payment-3" style={{width: 80, height: 72}}/>
                                    <h4 className="payment-title-properties">Sin comisiones</h4>
                                    <p className="payment-text-properties">No hay costes ocultos ni comisiones. Pagas por lo que vale el servicio, nada más</p>
                                </div>
                                </ScrollAnimation>
                            </div>

                        </div>

                    </section>

                    <ScrollableAnchor id={'section2'}>
                    <section id="descargar">
                        <ScrollAnimation animateIn="slideInUp" animateOnce= {true}>
                        <div className="descargar-feature" style={{position: 'relative'}}>
                            <h1 id="h1descargar">¡Descarga gratis tu aplicación de belleza favorita!</h1>
                            <div className="buttons" style={style.buttons}>
                                <span>
                                    <a href="https://itunes.apple.com/es/app/magnifique/id1252960188?mt=8"><img src="/img/app-store.png" alt="magnifique-ios" style={{width: '20%', height: '20%', display: 'inline-block', marginTop: 40}} /></a>
                                </span>
                                <span>
                                    <a href="https://play.google.com/store/apps/details?id=com.magnifique&hl=es"><img src="/img/play-store.png" alt="magnifique-play" style={{width: '20%', height: '20%', display: 'inline-block', marginLeft: 20, marginTop: 40}} /></a>
                                </span>
                            </div>
                        </div>
                        </ScrollAnimation>
                    </section>
                    </ScrollableAnchor>
                    <section id="footer">
                        <div className="text-footer" style={{textAlign: 'center'}}>
                            <p style={{color: '#b1b1b1', textAlign: 'center', fontWeight: 600, fontSize: 18, paddingTop: 75}}><span style={{color:'#00B1C6'}}>Magnifique Iberia S.L. </span>- © Todos los derechos reservados.- 2015</p>
                            <FlatButton
                                onClick={()=>this.redirect('facebook')}
                                icon={<img src="/img/facebook-icon.png" alt="Facebook" width="25" height="25"/>}
                            />
                            <FlatButton
                                onClick={()=>this.redirect('twitter')}
                                icon={<img src="/img/twitter-icon.png" alt="Twitter" width="25" height="25"/>}
                            />
                            <FlatButton
                                onClick={()=>this.redirect('instagram')}
                                icon={<img src="/img/instagram-icon.png" alt="Instagram" width="25" height="25"/>}
                            />
                        </div>
                    </section>
                </div>
            </div>
        )
    }
    
    componentDidMount() {
        var $S = require('scriptjs');
       $S('https://www.google.com/recaptcha/api.js', 'Recaptcha');
        this.getProfiles();
    }

    componentDidUpdate() {
        var userLang = navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2);
        reactTranslateChangeLanguage(userLang)
        //setTimeout(() => reactTranslateChangeLanguage(userLang), 50)
    }
}

export default HomePage;

