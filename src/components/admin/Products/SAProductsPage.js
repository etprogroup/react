import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import DataTables from 'material-ui-datatables';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SuperSelectField from '../../SuperSelectField'
import CircularProgress from 'material-ui/CircularProgress/CircularProgress'
import { Link } from 'react-router-dom';
import MultiUploader from '../../MultiUploader'
import {postImage,multiselectValueToArray,arrayToMultiselectValue} from '../../helpers'
import _arrayCopy from 'lodash._arraycopy';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import LinearProgress from 'material-ui/LinearProgress';
import {db} from '../../db'
import Snackbar from 'material-ui/Snackbar';
import XLSX from 'xlsx';
import Translate from 'translate-components'
import { reactTranslateChangeLanguage } from 'translate-components'

const styles={
    checkbox:{
        color:'#666'
    }
};

const TABLE_COLUMNS = [
    {
        key: 'brand',
        sortable: true,
        label: 'Marca',
        style:{width: '7%'}
    },{
        key: 'name',
        sortable: true,
        label: 'Nombre',
        style:{width: '29%'}
    },{
        key: 'size',
        sortable: true,
        label: 'Tamaño',
        style:{width: '10%'}
    }, {
        key: 'priceFormatted',
        sortable: true,
        label: 'Precio',
        style:{width: '8%'}
    }, {
        key: 'stock',
        sortable: true,
        label: 'Stock',
        style:{width: '5%'}
    }, {
        key: 'profile',
        sortable: true,
        label: 'Perfil',
        style:{width: '10%'}
    }, {
        key: 'line',
        sortable: true,
        label: 'Linea',
        style: {width: '16%'}
    }
];

const ERR_TABLE_COLUMNS = [
    {
        key: 'name',
        label: <Translate>Nombre</Translate>,
        style:{width: '20%'}
    }, {
        key: 'profile',
        label: <Translate>Perfil</Translate>,
        style:{width: '15%'}
    }, {
        key: 'price',
        label: <Translate>Precio</Translate>,
        style:{width: '15%'}
    }, {
        key: 'publico',
        label: <Translate>Público</Translate>,
        style:{width: '10%'}
    },{
        key: 'sex',
        label: <Translate>Sexo</Translate>,
        style:{width: '10%'}
    }
];

var numberOfImagesToUpload,imagesUploaded=0,savedImages=[];

class SAProductsPage extends Component{

    constructor(props){
        super(props);

        this.state = {
            page: 1,
            rowSize: 5,
            rowCount: 0,
            rowSizeList: [5,10,20,30],
            files: [],
            imagePreviewUrl: [],
            open: false,
            products:[],
            allProducts:[],
            product:{},
            newForm:true,
            nameerror:null,
            profileError:null,
            brandError:null,
            public:true,
            loading: false,
            type: 'ml',
            openSnackbar: false,
            
            openLoadProducts: false,
            loadProducts: false,
            loadProductsSummary: false,
            loadProductsFinished: false
        };
        this.saveImages = this.saveImages.bind(this);

        this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);

        this.saveNewProducts = this.saveNewProducts.bind(this);
    }

    addImageToPreview(image,file){
        var images=this.state.imagePreviewUrl;
        var files=this.state.files || [];
        images.push(image);
        files.push(file);
        this.setState({files:files,imagePreviewUrl:images });
    }

    closeNewproduct() {
        this.setState({ open: false });
    }

    handleRequestCloseSnackbar = () => {
        this.setState({
            openSnackbar: false,
        });
    };

    openNewproduct(slotInfo){
        imagesUploaded=0;
        savedImages = [];
        this.setState({files : []});
        this.getProfiles();
        this.setState({ open: true,imagePreviewUrl: [], product:{},newForm:true });
    }

    onRequestCloseLoadProductsDialog(){
        this.setState({
            openLoadProducts:false,
            loadProducts: false,
            loadProductsSummary: false,
            loadProductsFinished: false
        })
    }

    onFinishCloseLoadProductsDialog(){
        this.getproducts();
        this.setState({
            openLoadProducts:false,
            loadProducts: false,
            loadProductsSummary: false,
            loadProductsFinished: false
        })
    }

    saveNewProducts(){
        console.log("saveNewProducts")
        console.log(this.props)
        var products = this.state.newProductsLoaded;
        var token = this.props.token;
        var count = 0;
        var errorList = []

        if(products && products.length){
            for(var i = 0; i < products.length ; i++){
                var product = products[i];
                product.sex = product.sex && product.sex.toLowerCase() == 'Hombre' ? ['Hombre'] : ['Mujer']
                product.public = product.public && product.public.toLowerCase() == 'si' ? true : false
                product.price = product.price ? parseFloat((product.price).toString().replace(',','.')).toFixed(2) : null
                var body = {
                    ...product
                };

                console.log("body")
                console.log(body)
                db('post', '/api/products/', token, body)
                    .then((responseData) => {
                        if(responseData && responseData.status && responseData.status == 'OK'){
                            count = count + 1
                            this.setState({
                                countProductsInserted: count
                            })
                        }
                        else {
                            var detailsErr = responseData.fullError.op
                            detailsErr.publico = detailsErr.public ? 'Si' : 'No'
                            errorList.push(detailsErr)
                            this.setState({
                                countErrorProducts: errorList.length,
                                errorProductsList: errorList
                            })
                        }
                    })
                    .catch(function(err) {
                        console.log("error log en saveNewProduct of list");
                        console.log(err);
                    });
            }
            this.setState({
                countErrorProducts: errorList.length,
                loadProductsSummary:false,
                loadProductsFinished:true
            })
        }
    }

    openFileDialog = () => {
        var fileInputDom = ReactDOM.findDOMNode(this.refs.input)
        fileInputDom.click()
    };

    handleFileChange(e){
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.readAsBinaryString(file)
        reader.onloadend = () => {
            var fileData = reader.result;
            var wb = XLSX.read(fileData, {type : 'binary'});
            var rowObj = []
            var jsonObj = {}
            wb.SheetNames.forEach(function(sheetName){
                rowObj =XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
                jsonObj = JSON.stringify(rowObj);
            })
            console.log(rowObj)
            var allProducts = this.state.allProducts.map(function(value){ return value.name })
            var newProducts = rowObj.filter(function(value){ return allProducts.indexOf(value.name) == -1 })
            this.setState({
                loadProducts: false,
                loadProductsSummary: true,
                productsLoaded: rowObj,
                countProductsLoaded: rowObj.length,
                newProductsLoaded: newProducts,
                oldProductsLoaded: rowObj.length - newProducts.length
            })
        };
    };

    downloadTemplate(){
        /* original data */
        var data = [
            {"name":"ExcelProd1", "price": 10, "public":"no", "profile":"Peluquería", "sex":"Mujer"}
        ];

        /* make the worksheet */
        var ws = XLSX.utils.json_to_sheet(data);

        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Products");

        /* output format determined by filename */
        XLSX.writeFile(wb, 'template.xlsx');
        /* at this point, out.xlsb will have been downloaded */
    }

    getproducts(){
        this.setState({
            loading: true
        });
        db('get','/api/products/',this.props.token)
            .then((responseData) => {
                responseData.map(function (value) {
                    value.name = value.name ? value.name : '';
                    value.size = value.size ? value.size : '';
                    value.profile = value.profile ? value.profile : '';
                    value.line = value.line ? value.line : ''
                    value.brand = value.brand ? value.brand.name : ''
                    value.priceFormatted = value.price ? value.price.toFixed(2).toString().replace('.',',')+' €' : ''
                })

                this.setState({
                    products:responseData.slice(this.state.rowSize * (this.state.page - 1), this.state.rowSize * (this.state.page)),
                    allProducts:responseData,
                    rowCount : responseData.length,
                    loading: false
                })

            })
            .catch(function(e) {
                console.log(e);
                this.setState({
                    loading: false
                });
            });
    }

    getProfiles(){
        db('get','/api/profiles/',this.props.token)
            .then((responseData) => {

                var nodes = responseData.map(({name, _id}) =>
                    <div key={_id} value={name}>{name}</div>
                );
                this.setState({profiles:nodes})
            })
            .catch(function(e) {
                console.log(e);
            });
        db('get','/api/brands/',this.props.token)
            .then((responseData) => {

                var nodes = responseData.map(({name, _id}) =>
                    <div key={_id} value={name}>{name}</div>
                )
                this.setState({brands:nodes,allBrands:responseData})
            })
            .catch(function() {
                console.log("error");
            });
    }

    saveImages(imageUrl){
        if(typeof (imageUrl)==='string')savedImages.push(imageUrl);
        const {files} = this.state;
        numberOfImagesToUpload = files ? files.length : 0;
        if(numberOfImagesToUpload===imagesUploaded){
            this.saveNewproduct();
            return false;
        }
        postImage(files[imagesUploaded],this.saveImages)
        imagesUploaded++;
    }

    saveNewproduct(){
        this.setState({product:{...this.state.product,images:savedImages}});
        const {product} = this.state;
        var validation=true;

        if(product.name==null) {
            this.setState({nameerror: 'Este campo es obligatorio'})
            validation = false;
        }
        else{
            this.setState({nameerror:null})
        }
        if(product.profile==null){
            this.setState({profileError:(<div style={{color:'red'}}>Este campo es obligatorio</div>)})
            validation=false;
        }else{
            this.setState({profileError:null})
        }
        if(product.brand==null){
            this.setState({brandError:(<div style={{color:'red'}}>Este campo es obligatorio</div>)})
            validation=false;
        }else{
            product.brand=this.getBrandByName(product.brand)._id;
            this.setState({brandError:null})
        }
        console.log(product)
        if(product.sex){
            product.sex=multiselectValueToArray(product.sex);
        }else {
            product.sex=['Mujer']
        }

        if(product.price){
            product.price = product.price ? parseFloat((product.price).toString().replace(',','.')).toFixed(2) : null
        }
        product.shop = this.props.salon;

        if(validation){
            var method = this.state.newForm ? 'post' : 'put';
            db(method,'/api/products/' + (this.state.newForm ? '' : this.state.product._id),this.props.token,product)
                .then((jsonresponse) => {
                    console.log('ha entrado en el primer then')
                    if(jsonresponse){
                        if (jsonresponse.fullError && jsonresponse.fullError.code == 11000){
                            console.log('ha entrado en el if del jsonresponse')
                            this.setState({nameerror: 'Nombre repetido'})
                        }
                        else {
                            var methodSnack = this.state.newForm ? 'creado' : 'modificado';
                            this.setState({
                                open: false,
                                snackMessage: `Producto ${methodSnack} correctamente.`,
                                openSnackbar: true
                            });
                            this.getproducts();
                        }
                    }
                    else {
                        var methodSnackerror = this.state.newForm ? 'crear' : 'modificar';
                        this.setState({
                            open:false,
                            snackMessage:`Ha ocurrido un error al ${methodSnackerror} el producto.`,
                            openSnackbar:true,
                            loading:false
                        });
                    }
                })
                .catch(function(e) {
                    console.log(e);
                });
        }
    }

    deleteImage(src){

        const {product} = this.state

        const images = product.images.filter((v)=>{
            return v !== src;
        });
        var sex=null

        if(product.sex){
            sex=multiselectValueToArray(product.sex);
        }

        var body={
            images:images,
            name:product.name,
            sex:sex,
            profile:product.profile
        };
        db('put','/api/products/'+product._id,this.props.token,body)
            .then(results => {
                this.setState({imagePreviewUrl:images,product:{...this.state.product,images:images}})
            })
            .catch(function(e) {
                console.log(e);
            });
    }

    deleteproduct(){
        db('delete','/api/products/'+this.state.product._id,this.props.token)
            .then((response) => {
                if(response) {
                    this.setState({
                        open: false,
                        snackMessage: `Producto eliminado correctamente.`,
                        openSnackbar: true
                    });
                }
                else {
                    this.setState({
                        open:false,
                        snackMessage:`Ha ocurrido un error al eliminar el producto.`,
                        openSnackbar:true,
                        loading: false
                    });
                }
                this.getproducts();
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    getBrandByName=(name)=>{
        const {allBrands} = this.state;
        var brand = allBrands.filter(function (brandEl) {
            return name === brandEl.name;
        });
        if(brand.length===0)return null;
        return brand[0]
    };

    handleProfileSelection = (profile) => {
        this.setState({ product:{...this.state.product,profile:profile.value }})
    }

    handleBrandSelection = (brand) => {
        this.setState({ product:{...this.state.product,brand:brand.value }})
    }

    selectionsRenderer = (values, hintText, name) => {
        const { isFetchingStates } = this.state

        if (isFetchingStates) {
            return <div>
                <CircularProgress size={20} style={{marginRight: 10}}/>
                {hintText}
            </div>
        }


        if (!values) return hintText
        const { value, label } = values
        if (Array.isArray(values)) {
            return values.length
                ? values.map(({ value, label }) => label || value).join(', ')
                : hintText
        }
        else if (label || value) return label || value
        else return hintText
    }

    handleMultiSelection = (values, name) => this.setState({product: { ...this.state.product, [name]: values }})

    handleCellClick(y,x,row){
        imagesUploaded=0;
        savedImages = row.images ? _arrayCopy(row.images) : [];
        this.setState({
            files:[],
            imagePreviewUrl:_arrayCopy(savedImages),
            open:true,
            newForm:false,
            product:{...row, _id:row._id, sex:arrayToMultiselectValue(row.sex)}
        });
    }

    //functions pagination
    handlePreviousPageClick() {
        var currentPage = this.state.page
        this.setState({
            page: currentPage-1,
        });
        this.getproducts();
    }

    handleNextPageClick() {
        var currentPage = this.state.page
        this.setState({
            page: currentPage + 1,
        });
        this.getproducts();
    }

    onRowSizeChange =(a) => {
        var array = (this.state.allProductsFiltered && (this.state.allProductsFiltered.length !== this.state.allProducts.length)) ? this.state.allProductsFiltered : this.state.allProducts;
        this.setState({
            rowSize: this.state.rowSizeList[a],
            page: 1,
            products: array.slice(this.state.rowSizeList[a] * (0), this.state.rowSizeList[a] * (1)),
            rowCount : array.length
        });

    }

    // function for dynamic sorting
    compareValues(key, order='asc') {
        return function(a, b) {
            if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0;
            }
            var varA;
            var varB;

            if(key === 'priceFormatted') {
                varA = a['price'];
                varB = b['price'];
            }
            else{
                varA = (typeof a[key] === 'string') ?
                    a[key].toUpperCase() : a[key];
                varB = (typeof b[key] === 'string') ?
                    b[key].toUpperCase() : b[key];
            }

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order === 'desc') ? (comparison * -1) : comparison
            );
        }
    }

    handleSortOrderChange = (key, order) => {
        if(this.state.allProductsFiltered && (this.state.allProductsFiltered.length !== this.state.allProducts.length)){
            // hay filtros
            var array = this.state.allProductsFiltered.sort(this.compareValues(key, order)).slice(this.state.rowSize * (this.state.page - 1), this.state.rowSize * (this.state.page));
        }
        else{
            var array = this.state.allProducts.sort(this.compareValues(key, order)).slice(this.state.rowSize * (this.state.page - 1), this.state.rowSize * (this.state.page));
        }
        this.setState({products:array})
    }

    // function for dynamic filtering
    handleFilterValueChange = (value) => {
        var array = this.state.allProducts.filter(function (a) {
            var name =   a['name'] ?  a['name'] : '';
            var brand =   a['brand'] ?  a['brand'] : '';
            var priceFormatted =   a['priceFormatted'] ?  a['priceFormatted'] : '';
            var stock =   a['stock'] ?  a['stock'] : '';
            var profile =   a['profile'] ?  a['profile'] : '';
            var line =   a['line'] ?  a['line'] : '';
            var size =   a['size'] ?  a['size'] : '';
            return ( name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                brand.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                priceFormatted.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                stock.toString().toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                profile.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                line.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
                size.toLowerCase().indexOf(value.toLowerCase()) > -1) ? a : "";
        });
        this.setState({page:1, rowCount:array.length, allProductsFiltered:array})
        var slice = array.length > this.state.rowSize ? array.slice(this.state.rowSize * (this.state.page - 1), this.state.rowSize * (this.state.page)) : array;
        this.setState({products:slice})
    }

    render(){
        let imagePreviewUrl = this.state.imagePreviewUrl || [];
        let $imagePreview = null;

        const {product} = this.state;
        var checkbox = null;

        if (product.hasOwnProperty("public")) {
            checkbox = this.state.product.public;
        }
        else {checkbox = this.state.public}

        /*asignar el valor de la base de datos, y si es null asignarle el estado*/
        if (imagePreviewUrl) {
            const listOfImages = imagePreviewUrl.map((value,a) =>
                <div style={{float:'left'}}>
                    <FlatButton
                        style={{top:-50,backgroundColor:'rgba(256,256,256,0.8)'}}
                        label="Eliminar"
                        primary={true}
                        onTouchTap={()=>this.deleteImage(value)}
                    /><img
                    alt=""
                    key={a}
                    src={value}
                    style={
                        {maxWidth:200,left:-96,position:'relative'}
                    }/>

                </div>
            );
            $imagePreview = (
                <div style={{clear:'both'}}>
                    {listOfImages}
                </div>
            );
        }
//line is now defined
        var {profiles,profileError,brandError,brands,nameerror} = this.state;
        var {profile,price,brand,size,line} = this.state.product;


        profileError = profileError || 'Selecciona un perfil...';
        brandError = brandError || 'Selecciona una marca...';

        var formSex = this.state.product.sex || [{value:'Mujer'}];

        var saveBtnText = this.state.newForm ? 'Guardar' : 'Modificar';
        var dialogTitle = this.state.newForm ? "Añadir nuevo Producto" : 'Modificar Producto';
        var ssGeneralParmaeters = {
            multiple:true,
            checkPosition:'right',
            hintText:"Selección múltiple",
            unCheckedIcon:null,
            menuCloseButton:(<FlatButton label='Cerrar' hoverColor={'lightSalmon'} />),
            style:{ width: 256}
        };

        var actions = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onTouchTap={this.closeNewproduct.bind(this)}
            />,
            <FlatButton
                label={saveBtnText}
                primary={true}
                onTouchTap={this.saveImages}
            />
        ];

        if(!this.state.newForm)actions.push(
            <FlatButton
                label='Eliminar'
                secondary={true}
                onTouchTap={this.deleteproduct.bind(this)}
            />
        );

        var loadingContent;

        if (this.state.loading) {
            loadingContent = <div style={{marginTop:20, marginBottom:20, marginLeft:5}}> <LinearProgress mode="indeterminate" /> </div>;
        }

        var loadProductsActions = [
            <FlatButton
                label="No, descargar plantilla"
                primary={true}
                onTouchTap={this.downloadTemplate}
            />,
            <FlatButton
                label="Sí"
                primary={true}
                onTouchTap={this.openFileDialog}
            />,
            <FlatButton
                label="Cancelar"
                secondary={true}
                onTouchTap={this.onRequestCloseLoadProductsDialog.bind(this)}
            />
        ];

        if(this.state.loadProductsSummary){
            loadProductsActions.splice(0,3)
            loadProductsActions.push(
                <FlatButton
                    label="Sí"
                    primary={true}
                    onTouchTap={this.saveNewProducts}
                />,
                <FlatButton
                    label="No, cancelar importación"
                    secondary={true}
                    onTouchTap={this.onRequestCloseLoadProductsDialog.bind(this)}
                />
            )
        }

        if(this.state.loadProductsFinished){
            loadProductsActions.splice(0,3)
            loadProductsActions.push(
                <FlatButton
                    label="Cerrar"
                    secondary={true}
                    onTouchTap={this.onFinishCloseLoadProductsDialog.bind(this)}
                />
            )
        }
        var labelRowsPage = <Translate>Filas por página</Translate>

        return(
            <div className="full-width-container">
                <Snackbar
                    open={this.state.openSnackbar}
                    message={this.state.snackMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnackbar}
                />
                <h2 style={{padding:20,paddingBottom:0}}>PRODUCTOS</h2>
                <FloatingActionButton
                    secondary={true}
                    style={
                        {position:'absolute', marginLeft:180, marginTop:-70}
                    }
                    onTouchTap={() => this.openNewproduct()}
                >
                    <ContentAdd />
                </FloatingActionButton>
                <RaisedButton
                    label={<Translate>Cargar productos</Translate>}
                    secondary={true}
                    onClick={()=>this.setState({openLoadProducts: true, loadProducts: true})}
                    style={{float:'right', marginRight:20, marginTop:15}}
                />
                <Dialog
                    title={<p><Translate>Carga masiva de productos</Translate></p>}
                    titleClassName="dialog-title"
                    actions={loadProductsActions}
                    modal={true}
                    open={this.state.openLoadProducts}
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                    onRequestClose={this.onRequestCloseLoadProductsDialog}
                >
                    <div id="loadProducts" className={"collapse" + ((this.state.loadProducts) ? ' in' : '')}>
                        <h1 style={{textAlign:'center', fontSize:18}}>¿El archivo a importar tiene el formato estándar de Magnifique?</h1>
                    </div>
                    <div id="loadProductsSummary" className={"collapse" + ((this.state.loadProductsSummary) ? ' in' : '')}>
                        <h1 style={{textAlign:'center', fontSize:22, paddingBottom:20}}>¿Desea proseguir con la importación de clientes?</h1>
                        <p style={{fontWeight:'bold'}}>Resumen del análisis</p>
                        <ul>
                            <li>Productos detectados en archivo: {this.state.countProductsLoaded}</li>
                            <li>Productos ya existentes: {this.state.oldProductsLoaded}</li>
                            <li>Productos nuevos a insertar: {this.state.newProductsLoaded ? this.state.newProductsLoaded.length : 0}</li>
                        </ul>
                    </div>
                    <div id="loadProductsFinished" className={"collapse" + ((this.state.loadProductsFinished) ? ' in' : '')}>
                        <h1 style={{textAlign:'center', fontSize:28}}>Importación finalizada</h1>
                        <p style={{textAlign:'center'}}>Productos creados con éxito: {this.state.countProductsInserted}</p>
                        <p style={{textAlign:'center', fontWeight:'bold'}}>Con error: {this.state.countErrorProducts}</p>
                        { this.state.errorProductsList && <DataTables
                            height={'auto'}
                            selectable={false}
                            showRowHover={true}
                            columns={ERR_TABLE_COLUMNS}
                            data={this.state.errorProductsList}
                            showCheckboxes={false}
                            rowSizeLabel={labelRowsPage}

                            count={this.state.errorProductsList.length}
                            page={1}
                            rowSize={this.state.errorProductsList.length}
                            rowSizeList={this.state.rowSizeList}
                        /> }
                    </div>
                </Dialog>
                <form onSubmit={this._handleSubmit}>
                    <input id="inputFile" type="file" ref='input' style={{ display: 'none' }}
                           onChange={this.handleFileChange}/>
                </form>
                {loadingContent}
                <DataTables
                    height={'auto'}
                    selectable={false}
                    showRowHover={true}
                    columns={TABLE_COLUMNS}
                    data={this.state.products}
                    showCheckboxes={false}
                    showHeaderToolbar = {true}
                    showHeaderToolbarFilterIcon = {false}
                    headerToolbarMode = {'filter'}
                    onFilterValueChange={this.handleFilterValueChange}
                    onSortOrderChange={this.handleSortOrderChange}
                    onCellClick={this.handleCellClick.bind(this)}
                    rowSizeLabel={labelRowsPage}

                    count={this.state.rowCount}
                    page={this.state.page}
                    onRowSizeChange={this.onRowSizeChange}
                    onNextPageClick={this.handleNextPageClick}
                    onPreviousPageClick={this.handlePreviousPageClick}
                    rowSize={this.state.rowSize}
                    rowSizeList={this.state.rowSizeList}
                />
                <Dialog
                    title={dialogTitle}
                    titleClassName="dialog-title"
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                >
                    <div style={{float:'left'}}><br/><br/>
                        <SuperSelectField
                            name='brand'
                            floatingLabel='Marca*'
                            hintText={brandError}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText, 'brand')}
                            value={{value:brand}}
                            onChange={this.handleBrandSelection}
                            checkPosition='right'
                            style={{ width: 250 }}
                        >
                            {brands}
                        </SuperSelectField><br/><br/>
                        <Checkbox
                            label="Venta al público"
                            labelPosition="left"
                            checked={checkbox}
                            onCheck={(e, value)=>this.setState({ product: { ...this.state.product, public: value } })}
                            style={styles.checkbox}
                            iconStyle={styles.checkbox}
                            inputStyle={styles.checkbox}
                            labelStyle={styles.checkbox}
                        /><br/>
                    </div>
                    <div style={{float:'right'}}>
                        <TextField
                            fullWidth={true}
                            floatingLabelText="Nombre*"
                            errorText={nameerror}
                            name="name"
                            defaultValue={this.state.product.name}
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, name: value } })}
                            style={{ width: 256,marginTop:4 }}
                        /><br/>
                        <TextField
                            floatingLabelText="Línea del producto"
                            value={line}
                            name="line"
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, line: value } })}
                        />
                    </div>
                    <TextField
                        fullWidth={true}
                        floatingLabelText="Descripción"
                        multiLine={true}
                        name="description"
                        defaultValue={this.state.product.description}
                        onChange={(e, value)=>this.setState({ product: { ...this.state.product, description: value } })}
                    /><br/>
                    <hr style={{clear:'both',marginBottom:0}}/>
                    {/* Column 1 */}
                    <div style={{float:'left'}}>
                        <TextField
                            floatingLabelText="Precio"
                            value={price}
                            name="price"
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, price: value } })}
                        /><br/><br/>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='sex'
                            floatingLabel='Sexo'
                            value={formSex}
                            onChange={this.handleMultiSelection}
                            elementHeight={[36, 36]}
                        >
                            <div value='Hombre'>Hombre</div>
                            <div value='Mujer'>Mujer</div>
                        </SuperSelectField>
                        <TextField
                            floatingLabelText="Tamaño"
                            value={size}
                            name="size"
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, size: value } })}
                        /><br/>
                        <SelectField
                            value={this.state.type}
                            onChange={(e, index, value)=>this.setState({ type: value })}
                        >
                            <MenuItem value='ml' primaryText="ML" />
                            <MenuItem value='caps' primaryText="Cápsulas" />
                            <MenuItem value='g' primaryText="Gramos" />
                        </SelectField><br/>
                        <SuperSelectField
                            name='profiles'
                            floatingLabel='Perfil*'
                            hintText={profileError}
                            selectionsRenderer={(values, hintText) => this.selectionsRenderer(values, hintText, 'states')}
                            value={{value:profile}}
                            onChange={this.handleProfileSelection}
                            checkPosition='right'
                            style={{ width: 250, marginRight: 60 }}
                        >
                            {profiles}
                        </SuperSelectField><br/><br/>
                    </div>
                    {/* Column 2 */}
                    <div style={{float:'right'}}>
                        <TextField
                            floatingLabelText="Precio con descuento"
                            defaultValue={this.state.product.phoneNumber}
                            errorText={this.state.phoneNumbererror}
                            name="salePrice"
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, salePrice: value } })}
                        /><br/>
                        <TextField
                            floatingLabelText="Stock"
                            defaultValue={this.state.product.stock}
                            name="salePrice"
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, stock: value } })}
                            style={{marginTop:-18}}/>
                        <br/><br/>
                        <RaisedButton
                            label='Perfiles'
                            containerElement={<Link to="/perfiles" target="_blank"/>}
                            linkButton={true}
                        />
                    </div>
                    <hr style={{clear:'both'}}/>
                    <div style={{fontName:'helvetica-bd'}}>MEDIA</div>
                    <div>
                        <TextField
                            fullWidth={true}
                            floatingLabelText="Vídeos"
                            name="videos"
                            defaultValue={this.state.product.videos}
                            onChange={(e, value)=>this.setState({ product: { ...this.state.product, videos: [value] } })}
                        /><br/>
                        <div style={{float:'left'}}>
                            <MultiUploader
                                label='Escoger imágenes del producto'
                                onLoad={this.addImageToPreview.bind(this)}
                            />
                            <br/>
                        </div>
                        <br/>
                        {$imagePreview}
                    </div>
                </Dialog>
            </div>
        )
    }

    componentDidMount(){
        this.getproducts()
    }

    componentWillUpdate(){
    }
}

export default SAProductsPage;