﻿import React, { Component } from 'react';
import DataTables from 'material-ui-datatables';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import SuperSelectField from '../../SuperSelectField'
import {multiselectValueToArray} from '../../helpers'
import {arrayToMultiselectValue} from '../../helpers'
import {DOMAIN} from '../../../config'

const TABLE_COLUMNS = [
    {
        key: 'name',
        label: 'Nombre'
    }, {
        key: 'salonType',
        label: 'Tipo de Salón'
    }
];

class profilesPage extends Component{

    constructor(props){
        super(props);

        this.state = {
            open: false,
            profiles:[],
            profile:{},
            newForm:true,
            nameerror:null,
        };
    }

    closeNewprofile() {
        this.setState({ open: false });
    }

    openNewprofile(slotInfo){
        this.setState({ open: true, profile:{
            profiles:[],
            shop:this.props.salon
        },newForm:true });
    }

    getprofiles(){
        fetch(
            DOMAIN+'/api/profiles/', {
                method: 'get',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer '+this.props.token
                }
            })
            .then((response) =>
            {
                return response.json();
            })
            .then((responseData) => {
                this.setState({profiles:responseData})
            })
            .catch(function() {
                console.log("error");
            });
    }

    saveNewprofile(){
        if(this.state.profile.name==null){
            this.setState({nameerror:'Este campo es obligatorio'})
        }else{
            this.setState({nameerror:null})
        }

        if(this.state.profile.name!=null){
            var url = DOMAIN+'/api/profiles/' + (this.state.newForm ? '' : this.state.profile._id);
            var method = this.state.newForm ? 'post' : 'put';
            fetch(
                url, {
                    method: method,
                    dataType: 'json',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization':'Bearer '+this.props.token
                    },
                    body: JSON.stringify(this.state.profile)
                })
                .then(() =>
                {
                    this.getprofiles()
                    this.setState({open:false})
                })
                .catch(function() {
                    console.log("error");
                });
        }
    }

    deleteprofile(){
        fetch(
            DOMAIN+'/api/profiles/'+this.state.profile._id, {
                method: 'delete',
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer '+this.props.token
                }
            })
            .then(() =>
            {
                this.getprofiles()
                this.setState({open:false})
            })
            .catch(function() {
                console.log("error");
            });
    }

    handleMultiSelection = (values, name) => this.setState({profile: { ...this.state.profile, [name]: values }})

    handleCellClick(y,x,row){
        this.setState({
            open:true,
            newForm:false,
            profile:{...row}
        })
    }

    render(){
        var formSalon = typeof (this.state.profile.salonType) == 'undefined' ? 'Peluqueria' : this.state.profile.salonType[0];
        var formHairType = this.state.profile.hairType || [];
        var formHairTexture = this.state.profile.hairTexture || [];

        var saveBtnText = this.state.newForm ? 'Guardar' : 'Modificar';
        var dialogTitle = this.state.newForm ? "Añadir nuevo Perfil" : 'Modificar Perfil';

        var hairColor = this.state.profile.hairColor || [];
        var hairState = this.state.profile.hairState || [];
        var nailType = this.state.profile.nailType || [];
        var nailPolish = this.state.profile.nailPolish || [];
        var skinType = this.state.profile.skinType || [];
        var eyebrowType = this.state.profile.eyebrowType || [];
        var eyelashType = this.state.profile.eyelashType || [];

        var ssGeneralParmaeters = {
            multiple:true,
            checkPosition:'right',
            hintText:"Selección múltiple",
            unCheckedIcon:null,
            menuCloseButton:(<FlatButton label='Cerrar' hoverColor={'lightSalmon'} />),
            style:{ width: 256}
    };
        console.log(formSalon)

        var actions = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onTouchTap={this.closeNewprofile.bind(this)}
            />,
            <FlatButton
                label={saveBtnText}
                primary={true}
                onTouchTap={this.saveNewprofile.bind(this)}
            />
        ];

        if(!this.state.newForm)actions.push(
            <FlatButton
                label='Eliminar'
                secondary={true}
                onTouchTap={this.deleteprofile.bind(this)}
            />
        );

        return(
            <div className="full-width-container">
                <h2 style={{padding:20,paddingBottom:0}}>PERFILES</h2>
                <FloatingActionButton
                    secondary={true}
                    style={
                        {position:'absolute', marginLeft:180, marginTop:-70}
                    }
                    onTouchTap={() => this.openNewprofile()}
                >
                    <ContentAdd />
                </FloatingActionButton>
                <DataTables
                    height={'auto'}
                    selectable={false}
                    showRowHover={true}
                    columns={TABLE_COLUMNS}
                    data={this.state.profiles}
                    showCheckboxes={false}
                    onCellClick={this.handleCellClick.bind(this)}
                    rowSizeLabel="Filas por página"
                />
                <Dialog
                    title={dialogTitle}
                    titleClassName="dialog-title"
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                    contentStyle={{width:660}}
                    autoScrollBodyContent={true}
                >
                    <div>
                        <TextField
                            fullWidth={true}
                            floatingLabelText="Nombre*"
                            errorText={this.state.emailerror}
                            name="name"
                            defaultValue={this.state.profile.name}
                            onChange={(e, value)=>this.setState({ profile: { ...this.state.profile, name: value } })}
                        /><br/>
                        <SelectField
                            floatingLabelText='Tipo de Salón'
                            value={formSalon}
                            onChange={(e, i, value)=>this.setState({ profile: { ...this.state.profile, salonType: value } })}
                        >
                            <MenuItem value={'Peluqueria'} primaryText="Peluquería" />
                            <MenuItem value={'Estetica'} primaryText="Estética" />
                            <MenuItem value={'Tattoo'} primaryText="Tattoo" />
                        </SelectField><br/>
                    </div>

                    <hr style={{clear:'both'}}/>
                    <h2>Pelo</h2><br/>
                    {/* Column 1 */}
                    <div style={{float:'left'}}>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='hairType'
                            floatingLabel='Tipo de Pelo'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, hairType:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(formHairType)}
                            elementHeight={[36,36,36]}
                        >
                            <div value='Fino'>Fino</div>
                            <div value='Medio'>Medio</div>
                            <div value='Grueso'>Grueso</div>
                        </SuperSelectField><br/><br/>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='hairColor'
                            floatingLabel='Color de Pelo'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, hairColor:multiselectValueToArray(value) }} )}
                            value={arrayToMultiselectValue(hairColor)}
                            elementHeight={[36,36,36]}
                        >
                            <div value='Rubio'>Rubio</div>
                            <div value='Rojo/Cobrizo'>Rojo/Cobrizo</div>
                            <div value='Castaño/Negro'>Castaño/Negro</div>
                        </SuperSelectField><br/><br/>
                    </div>
                    {/* Column 2 */}
                    <div style={{float:'right'}}>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='hairTexture'
                            floatingLabel='Textura de Pelo'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, hairTexture: multiselectValueToArray(value)} })}
                            value={arrayToMultiselectValue(formHairTexture)}
                            elementHeight={[36,36,36]}
                        >
                            <div value='Liso'>Liso</div>
                            <div value='Ondulado'>Ondulado</div>
                            <div value='Rizado'>Rizado</div>
                        </SuperSelectField><br/><br/>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='hairState'
                            floatingLabel='Estado de Pelo'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, hairState:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(hairState)}
                            elementHeight={[36,36]}
                        >
                            <div value='Virgen'>Virgen</div>
                            <div value='Dañado'>Dañado</div>
                        </SuperSelectField><br/>
                    </div>
                    <hr style={{clear:'both'}}/>
                    <h2>Uñas</h2><br/>
                    {/* Column 1 */}
                    <div style={{float:'left'}}>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='nailType'
                            floatingLabel='Tipo de Uñas'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, nailType:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(nailType)}
                            elementHeight={[36,36,36,36]}
                        >
                            <div value='Naturales'>Naturales</div>
                            <div value='Postizas'>Postizas</div>
                            <div value='Debil'>Débil</div>
                            <div value='Fuerte'>Fuerte</div>
                        </SuperSelectField><br/><br/>
                    </div>
                    {/* Column 2 */}
                    <div style={{float:'right'}}>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='nailPolish'
                            floatingLabel='Esmaltado de Uñas'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, nailPolish:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(nailPolish)}
                            elementHeight={[36,36]}
                        >
                            <div value='Normal'>Normal</div>
                            <div value='Semipermanente'>Semipermanente</div>
                        </SuperSelectField><br/><br/>
                    </div>
                    <hr style={{clear:'both'}}/>
                    {/* Column 1 */}
                    <div style={{float:'left'}}>
                        <h2>Piel</h2><br/>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='skinType'
                            floatingLabel='Tipo de Piel'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, skinType:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(skinType)}
                            elementHeight={[36,36,36]}
                        >
                            <div value='Grasa'>Grasa</div>
                            <div value='Seca'>Seca</div>
                            <div value='Mixta'>Mixta</div>
                        </SuperSelectField><br/><br/>
                    </div>

                    {/* Column 1 */}
                    <div style={{float:'right'}}>
                        <h2>Cejas</h2><br/>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='eyebrowType'
                            floatingLabel='Tipo de Cejas'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, eyebrowType:multiselectValueToArray(value) } })}
                            value={arrayToMultiselectValue(eyebrowType)}
                            elementHeight={[36,36,36,36,36,36,36,36,36]}
                        >
                            <div value='Pobladas'>Pobladas</div>
                            <div value='Despobladas'>Despobladas</div>
                            <div value='Claras'>Claras</div>
                            <div value='Oscuras'>Oscuras</div>
                            <div value='Naturales'>Naturales</div>
                            <div value='Tatuadas'>Tatuadas</div>
                            <div value='Microbladding'>Microbladding</div>
                            <div value='Micropigmentacion'>Micropigmentación</div>
                            <div value='Tinte'>Tinte</div>
                        </SuperSelectField><br/><br/>
                    </div>
                    <hr style={{clear:'both'}}/>
                    <h2>Pestañas</h2><br/>
                    {/* Column 1 */}
                    <div style={{float:'left'}}>
                        <SuperSelectField
                            {...ssGeneralParmaeters}
                            name='eyelashType'
                            floatingLabel='Tipo de Pestañas'
                            onChange={(value)=>this.setState({ profile: { ...this.state.profile, eyelashType:multiselectValueToArray(value) } })}
                            value={eyelashType}
                            elementHeight={[36,36,36,36,36,36,36]}
                        >
                            <div value='Pobladas'>Pobladas</div>
                            <div value='Cortas'>Cortas</div>
                            <div value='Largas'>Largas</div>
                            <div value='Rizadas'>Rizadas</div>
                            <div value='Rectas'>Rectas</div>
                            <div value='Finas'>Finas</div>
                            <div value='Gruesas'>Gruesas</div>
                        </SuperSelectField><br/><br/>
                    </div>
                </Dialog>
            </div>
        )
    }

    componentDidMount(){
        this.getprofiles()
    }

    componentWillUpdate(){
    }
}

const styles={
    checkbox:{
        color:'#666'
    }
}

export default profilesPage;